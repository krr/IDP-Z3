---
title: "Install IDP-Z3"
# meta title
meta_title: ""
# meta description
description: "Installation for IDP-Z3."
# save as draft
draft: false
---

The way to install IDP-Z3 depends on what components you need.

### Using without installation

You can opt to use IDP-Z3 without installing anything through the online environment.
You can open them in a new tab by clicking on the button belo.

{{< button label="Online Environment" link="https://interactive-consultant.idp-z3.be/" style="solid" >}}

{{< notice "info" >}}
There are timeouts in place for the online editors -- if model really difficult problems, you might hit them. When this occurs, it is best to install IDP-Z3 locally.
{{< /notice >}}


### IDP-Z3 core

If you only require the core engine, for instance to integrate it in a Python project, you can install it via Pip:

<pre class="text-black">
<code> pip3 install idp-engine </code>
</pre>

This also installs a command-line interface for IDP-Z3, which you can call as follows:

<pre class="text-black">
<code>
usage: idp-engine [-h] [--version] [-o OUTPUT] [--full-formula] [--no-timing] FILE

IDP-Z3

positional arguments:
  FILE                  path to the .idp file

options:
  -h, --help            show this help message and exit
  --version, -v         show program's version number and exit
  -o OUTPUT, --output OUTPUT
                        name of the output file
  --full-formula        show the full formula
  --no-timing           don't display timing information
</code>
</pre>


### Interactive Consultant

If you wish to also install the Interactive Consultant alongside IDP-Z3, you can follow the more detailed installation guide contained in our code repository.

{{< button label="IC installation guide" link="https://gitlab.com/krr/IDP-Z3#installation" style="solid" >}}


