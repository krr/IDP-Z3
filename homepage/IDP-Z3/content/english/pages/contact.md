---
title: "Contact us"
# meta title
meta_title: ""
# meta description
description: "Contact page for IDP-Z3."
# save as draft
draft: false
---

# Looking to contact us?

IDP-Z3 is an open-source project maintained and developed by a passionate team at the KU Leuven University. We are always happy to hear from you. You can contact us in multiple ways:

* [Matrix chat](https://matrix.to/#/#IDP-Z3:matrix.org)
* [Email](mail:krr@cs.kuleuven.be)
* [Gitlab issues](https://gitlab.com/krr/IDP-Z3/-/issues)

(We are also always actively looking for collaborations with industry – feel free to reach out to us if you have an interesting idea!)
