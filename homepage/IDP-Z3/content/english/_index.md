---
# Banner
banner:
  # title: "The IDP-Z3 Reasoning Engine"
  title: "Use your domain knowledge to build intelligent systems"
  content: "IDP-Z3 and its ecosystem can help you leverage domain knowledge to streamline your processes in a **declarative**, **versatile** and **explainable** manner."
  image: "/images/banner.png"
  button:
    enable: false
    label: "Get Started"
    link: "https://github.com/zeon-studio/hugoplate"
  key1:
    label: "Declarative"
    text: "Expressing your knowledge about a particular domain is much simpler than writing programs. IDP-Z3 understands familiar mathematical notation and easy-to-use Excel-like tables."
    link: "/brol"
  key2:
    label: "Versatile"
    text: "In our daily lives, we often use the same knowledge to perform very different tasks. IDP-Z3 is also able to re-use knowledge to solve very different problems, making it multi-functional."
  key3:
    label: "Explainable"
    text: "IDP-Z3 uses only logic: there are no black-box models! Whenever IDP-Z3 gives you any piece of information, it can be traced back to its source and explained."


# Features
features:
  - title: "Knowledge Base System"
    image: "/images/undraw_engineer.svg"
    content: "IDP-Z3 is a reasoning engine for domain knowledge. Using a formal specification of your problem domain, it can perform many useful inference tasks to drive intelligent behavior and support your users in their processes:"
    bulletpoints:
      - "Verifying conformance;"
      - "Generating possible solutions;"
      - "Deriving consequences;"
      - "Explaining why a fact can (not) be derived;"
      - "Ask relevant questions;"
      - "And more."
    button:
      enable: false
      label: "Get Started Now"
      link: "#"

  - title: "Your Knowledge, Your Power"
    image: "/images/undraw_learning.svg"
    content: "Domain knowledge is valuable, but can be tricky to capture. Our toolbox offers different notations to aid you in this formalization process, allowing you to find one that matches for you and your team."
    bulletpoints:
      - "FO(·), our representation language based on first-order logic."
      - "cDMN, a tabular-based notation familiar to those with knowledge of Excel."
      - "CNL, which bridges the gap between natural and formal language."
      - "Feature Models, a graphical representation for product configurations."
      - "The list goes on!"
    button:
      enable: false
      label: "Get Started Now"
      link: ""

  - title: "Quick prototyping"
    image: "/images/undraw_prototype.svg"
    content: "Our Interactive Consultant (IC) makes building prototypes incredibly straightforward. Simply plunk in your knowledge, and you are good to go. No programming required!"
    bulletpoints:
    button:
      enable: false
      label: "Quick start"
      link: ""

learnbutton:
  label: "Ready to learn more?"
  link: "/learn"
---
