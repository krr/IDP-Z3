try:
    from idp_web_server.rest import app
except (ImportError, ModuleNotFoundError) as e:
    import re, sys
    if isinstance(e, ImportError):
        package = re.findall(r"from '(\w+?)'", str(e))[0]
    else:
        package = re.findall(r"from '(\w?)'", str(e))[0]
    print(f"Missing package \"{package}\"."
           f" Did you install the consultant dependencies?"
           f" You can do so as follows:\n\n"
           f"poetry install --with consultant\n\n"
           f"Refer to the README for more information")
    sys.exit(-1)

if __name__ == '__main__':
    app.run(debug=True)
