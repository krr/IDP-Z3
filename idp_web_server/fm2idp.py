"""
Convert (typed) Feature Models in json format to IDP's FO(.) using the
semantics as described by Callewaert et al. (2024).

Authors: Simon Vandevelde <s.vandevelde@kuleuven.be>,
Benjamin Callewaert <benjamin.callewaert@kuleuven.be>
"""

import argparse
import json

# Maps the built-in types on a "none value".
NONE_DICT = {"Int": "nan()", "Real": "nar()", "Date": "nad()"}


def none_value(type_: str) -> str:
    """
    Returns the correct none value for a specific type.
    Each type has a specific non-value associated with it, used whenever a
    feature is not included.
    """
    return NONE_DICT[type_] if type_ in NONE_DICT else f"no_{type_}"


def included_node(node_name: str, node_type: str) -> str:
    """
    Returns FO(.) which represents when a node is included.
    This changes depending on whether the node is typed or not.
    """
    if node_type == "":
        return f"{node_name}()"
    else:
        return f"{node_name}() ≠ {none_value(node_type)}"


def fm_to_fodot(json_data: dict) -> str:
    """Converts feature model into FO(.) format"""
    types = json_data["_type_declarations"]

    voc = "vocabulary {\n\tnan:() → Int \n\tnad:() → Date \n\tnar:() → Real\n"
    theory = "theory {\n"
    display = "display {\n"

    # First, add the custom types to the vocabulary (and introduce a
    # non-value for each type).
    voc += "\n".join(
        [
            f"\ttype {x['name']} := {{{x['values']}, {none_value(x['name'])}}}"
            for x in types
        ]
    )

    # Then, loop over each featurenode and:
    # * Declare it in the vocabulary
    # * Add a formula to the theory describing each child relation
    # * Add a formula to the theory describing the parent relation
    for node_name, node in json_data.items():
        if node_name.startswith("_"):
            continue

        # Declare the node in the voc
        n_type = node["Type"] if node["Type"] != "" else "Bool"
        voc += f"\t{node_name}: -> {n_type} \n"

        parent = included_node(node_name, node["Type"])
        children = []
        for child, child_relation in node["Children"].items():
            # If the child relation is MANdatory, the child and parent imply
            # each other.
            # Else, the child always implies the parent (as the child cannot be
            # included if the parent isn't either)
            child_p = included_node(child, json_data[child]["Type"])
            children.append(child_p)
            if child_relation == "MAN":
                theory += f"\t{parent} <=> {child_p}.\n"
            else:
                theory += f"\t{parent} <= {child_p}.\n"

        # If the parent relation is OR, at least one of the children should be
        # present.
        # If the parent relation is ALTernative, exactly one child may be
        # present.
        # If the parent relation is AND, nothing needs to be added.
        if node["Relation"] == "OR":
            theory += f'\t{parent} => {" | ".join(children)}.\n'
        elif node["Relation"] == "ALT":
            for i, child in enumerate(children):
                other_children = children[:i] + children[i + 1 :]
                constraint = (
                    f'{parent} => ({child} <=> ~({" | ".join(other_children)})).'
                )
                theory += f"\t{constraint}\n"

        # Also add the children under the same header for the IC.
        if node["Children"].items():
            child_names = ["`" + child for child in node["Children"].keys()]
            display += (
                f"\theading('{node_name} children'," f"{','.join(child_names)}).\n"
            )
    theory += "}\n"
    display += "}\n"
    structure = (
        "structure {\n\tnan := -10000.\n\tnar := -10000.0.\n\tnad :=  #1970-01-01.\n}\n"
    )
    procedure = "procedure main() { pretty_print(model_expand(T, S))}\n"
    return voc + "}\n" + theory + structure + display + procedure


def main():
    parser = argparse.ArgumentParser(description="Convert FM to IDP")
    parser.add_argument("json", type=str, help="the source json file")
    parser.add_argument("output", type=str, help="the target idp file")
    args = parser.parse_args()

    with open(args.json, "r") as fp:
        data = json.load(fp)

    idp = fm_to_fodot(data)
    with open(args.output, "w") as fp:
        fp.write(idp)


if __name__ == "__main__":
    main()
