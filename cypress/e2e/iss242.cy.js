describe('template spec', () => {
    it('passes', () => {
      cy.visit('http://localhost:4201/')
      cy.get('.p-dialog-titlebar').find('a').click();
      cy.contains('File').click()
      cy.contains('Polygon').click()
      cy.get('.p-dialog-titlebar').find('a').click();
      cy.get('.p-panel-title').contains('sides') // wait for loading

      cy.assert_false('irregular');
      cy.assert_true('regular triangle');
      cy.retract('irregular');
      cy.retract('regular triangle');

    cy.is_unset('regular triangle');
    })
  })