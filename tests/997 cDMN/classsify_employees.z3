
-- original ---------------------------------


vocabulary V {
	type Person_t := { Røbinson, Warner, Stevens, White, Smith, Green, Brown, Ǩlaus, Houston, Long, Short, Doé }
	type Marital_Status_t := { Married, Single }
	type Gender_t := { Male, Female }
	type Age_t := { 0..100 }
	status_of_Person: Person_t -> Marital_Status_t
	gender_of_Person: Person_t -> Gender_t
	age_of_Person: Person_t -> Real
	salary_of_Person: Person_t -> Real
	Minimal_Salary:  -> Real
	Maximal_Salary:  -> Real
	Average_Salary:  -> Real
	Total_Salary:  -> Real
	Person_is_Rich: Person_t -> Bool
}


structure S: V {
	status_of_Person := {(Røbinson) -> Married, (Warner) -> Married, (Stevens) -> Single, (White) -> Married, (Smith) -> Single, (Green) -> Married, (Brown) -> Married, (Ǩlaus) -> Married, (Houston) -> Single, (Long) -> Married, (Short) -> Single, (Doé) -> Single}.
	gender_of_Person := {(Røbinson) -> Female, (Warner) -> Male, (Stevens) -> Male, (White) -> Female, (Smith) -> Male, (Green) -> Female, (Brown) -> Male, (Ǩlaus) -> Male, (Houston) -> Female, (Long) -> Male, (Short) -> Male, (Doé) -> Female}.
	age_of_Person := {(Røbinson) -> 25, (Warner) -> 45, (Stevens) -> 24, (White) -> 32, (Smith) -> 46, (Green) -> 28, (Brown) -> 32, (Ǩlaus) -> 54, (Houston) -> 47, (Long) -> 29, (Short) -> 22, (Doé) -> 21}.
	salary_of_Person := {(Røbinson) -> 20000, (Warner) -> 150000, (Stevens) -> 35000, (White) -> 75000, (Smith) -> 110000, (Green) -> 40000, (Brown) -> 65000, (Ǩlaus) -> 85000, (Houston) -> 35000, (Long) -> 40000, (Short) -> 20000, (Doé) -> 21000}.
}
theory T: V {
	//Minimal Salary
	Minimal_Salary() = min{{ (salary_of_Person(Person)) | Person in Person_t: true}}.
	//Maximal Salary
	Maximal_Salary() = max{{ (salary_of_Person(Person)) | Person in Person_t: true}}.
	//Total Salary
	Total_Salary() = sum{{ (salary_of_Person(Person)) | Person in Person_t: true}}.


	[]
	{

		Average_Salary() = Total_Salary()/#{ typevar ∈ Person_t : true} <- true.
	}
	[Average Salary OR Rich Employees]
	{

		!Person in Person_t: Person_is_Rich(Person) <- salary_of_Person(Person) > 85000.
	}
}

-- meta -------------------------------------
{'introduction': '',
 'manualPropagation': False,
 'manualRelevance': False,
 'optionalPropagation': False,
 'optionalRelevance': False,
 'symbols': [{'environmental': False,
              'idpname': 'status_of_Person',
              'priority': 'core',
              'showOptimize': True,
              'type': 'function',
              'view': 'normal'},
             {'environmental': False,
              'idpname': 'gender_of_Person',
              'priority': 'core',
              'showOptimize': True,
              'type': 'function',
              'view': 'normal'},
             {'environmental': False,
              'idpname': 'age_of_Person',
              'priority': 'core',
              'showOptimize': True,
              'type': 'function',
              'view': 'normal'},
             {'environmental': False,
              'idpname': 'salary_of_Person',
              'priority': 'core',
              'showOptimize': True,
              'type': 'function',
              'view': 'normal'},
             {'environmental': False,
              'idpname': 'Minimal_Salary',
              'priority': 'core',
              'showOptimize': True,
              'type': 'function',
              'view': 'normal'},
             {'environmental': False,
              'idpname': 'Maximal_Salary',
              'priority': 'core',
              'showOptimize': True,
              'type': 'function',
              'view': 'normal'},
             {'environmental': False,
              'idpname': 'Average_Salary',
              'priority': 'core',
              'showOptimize': True,
              'type': 'function',
              'view': 'normal'},
             {'environmental': False,
              'idpname': 'Total_Salary',
              'priority': 'core',
              'showOptimize': True,
              'type': 'function',
              'view': 'normal'},
             {'environmental': False,
              'idpname': 'Person_is_Rich',
              'priority': 'core',
              'showOptimize': True,
              'type': 'function',
              'view': 'normal'}],
 'title': 'Interactive Consultant'}

-- propagation ------------------------------
{' Global': {'env_dec': False},
 'Average_Salary': {'Average_Salary()': {'environmental': False,
                                         'is_assignment': True,
                                         'nb_possible': -1,
                                         'normal': True,
                                         'reading': 'Average_Salary',
                                         'relevant': False,
                                         'status': 'UNIVERSAL',
                                         'typ': 'Real',
                                         'value': '58000'}},
 'Maximal_Salary': {'Maximal_Salary()': {'environmental': False,
                                         'is_assignment': True,
                                         'nb_possible': -1,
                                         'normal': True,
                                         'reading': 'Maximal_Salary',
                                         'relevant': False,
                                         'status': 'UNIVERSAL',
                                         'typ': 'Real',
                                         'value': '150000'},
                    'Maximal_Salary() = max{(salary_of_Person(Person)) | Person ∈ Person_t: true}': {'environmental': False,
                                                                                                     'is_assignment': False,
                                                                                                     'nb_possible': -1,
                                                                                                     'normal': False,
                                                                                                     'reading': 'Maximal_Salary '
                                                                                                                '= '
                                                                                                                'max{(salary_of_Person(Person)) '
                                                                                                                '| '
                                                                                                                'Person '
                                                                                                                '∈ '
                                                                                                                'Person_t: '
                                                                                                                'true}',
                                                                                                     'relevant': False,
                                                                                                     'status': 'UNIVERSAL',
                                                                                                     'typ': 'Bool',
                                                                                                     'value': True}},
 'Minimal_Salary': {'Minimal_Salary()': {'environmental': False,
                                         'is_assignment': True,
                                         'nb_possible': -1,
                                         'normal': True,
                                         'reading': 'Minimal_Salary',
                                         'relevant': False,
                                         'status': 'UNIVERSAL',
                                         'typ': 'Real',
                                         'value': '20000'},
                    'Minimal_Salary() = min{(salary_of_Person(Person)) | Person ∈ Person_t: true}': {'environmental': False,
                                                                                                     'is_assignment': False,
                                                                                                     'nb_possible': -1,
                                                                                                     'normal': False,
                                                                                                     'reading': 'Minimal_Salary '
                                                                                                                '= '
                                                                                                                'min{(salary_of_Person(Person)) '
                                                                                                                '| '
                                                                                                                'Person '
                                                                                                                '∈ '
                                                                                                                'Person_t: '
                                                                                                                'true}',
                                                                                                     'relevant': False,
                                                                                                     'status': 'UNIVERSAL',
                                                                                                     'typ': 'Bool',
                                                                                                     'value': True}},
 'Person_is_Rich': {'Person_is_Rich(Brown)': {'environmental': False,
                                              'is_assignment': False,
                                              'nb_possible': -1,
                                              'normal': True,
                                              'reading': 'Person_is_Rich(Brown)',
                                              'relevant': False,
                                              'status': 'UNIVERSAL',
                                              'typ': 'Bool',
                                              'value': False},
                    'Person_is_Rich(Doé)': {'environmental': False,
                                            'is_assignment': False,
                                            'nb_possible': -1,
                                            'normal': True,
                                            'reading': 'Person_is_Rich(Doé)',
                                            'relevant': False,
                                            'status': 'UNIVERSAL',
                                            'typ': 'Bool',
                                            'value': False},
                    'Person_is_Rich(Green)': {'environmental': False,
                                              'is_assignment': False,
                                              'nb_possible': -1,
                                              'normal': True,
                                              'reading': 'Person_is_Rich(Green)',
                                              'relevant': False,
                                              'status': 'UNIVERSAL',
                                              'typ': 'Bool',
                                              'value': False},
                    'Person_is_Rich(Houston)': {'environmental': False,
                                                'is_assignment': False,
                                                'nb_possible': -1,
                                                'normal': True,
                                                'reading': 'Person_is_Rich(Houston)',
                                                'relevant': False,
                                                'status': 'UNIVERSAL',
                                                'typ': 'Bool',
                                                'value': False},
                    'Person_is_Rich(Long)': {'environmental': False,
                                             'is_assignment': False,
                                             'nb_possible': -1,
                                             'normal': True,
                                             'reading': 'Person_is_Rich(Long)',
                                             'relevant': False,
                                             'status': 'UNIVERSAL',
                                             'typ': 'Bool',
                                             'value': False},
                    'Person_is_Rich(Røbinson)': {'environmental': False,
                                                 'is_assignment': False,
                                                 'nb_possible': -1,
                                                 'normal': True,
                                                 'reading': 'Person_is_Rich(Røbinson)',
                                                 'relevant': False,
                                                 'status': 'UNIVERSAL',
                                                 'typ': 'Bool',
                                                 'value': False},
                    'Person_is_Rich(Short)': {'environmental': False,
                                              'is_assignment': False,
                                              'nb_possible': -1,
                                              'normal': True,
                                              'reading': 'Person_is_Rich(Short)',
                                              'relevant': False,
                                              'status': 'UNIVERSAL',
                                              'typ': 'Bool',
                                              'value': False},
                    'Person_is_Rich(Smith)': {'environmental': False,
                                              'is_assignment': False,
                                              'nb_possible': -1,
                                              'normal': True,
                                              'reading': 'Person_is_Rich(Smith)',
                                              'relevant': False,
                                              'status': 'UNIVERSAL',
                                              'typ': 'Bool',
                                              'value': True},
                    'Person_is_Rich(Stevens)': {'environmental': False,
                                                'is_assignment': False,
                                                'nb_possible': -1,
                                                'normal': True,
                                                'reading': 'Person_is_Rich(Stevens)',
                                                'relevant': False,
                                                'status': 'UNIVERSAL',
                                                'typ': 'Bool',
                                                'value': False},
                    'Person_is_Rich(Warner)': {'environmental': False,
                                               'is_assignment': False,
                                               'nb_possible': -1,
                                               'normal': True,
                                               'reading': 'Person_is_Rich(Warner)',
                                               'relevant': False,
                                               'status': 'UNIVERSAL',
                                               'typ': 'Bool',
                                               'value': True},
                    'Person_is_Rich(White)': {'environmental': False,
                                              'is_assignment': False,
                                              'nb_possible': -1,
                                              'normal': True,
                                              'reading': 'Person_is_Rich(White)',
                                              'relevant': False,
                                              'status': 'UNIVERSAL',
                                              'typ': 'Bool',
                                              'value': False},
                    'Person_is_Rich(Ǩlaus)': {'environmental': False,
                                              'is_assignment': False,
                                              'nb_possible': -1,
                                              'normal': True,
                                              'reading': 'Person_is_Rich(Ǩlaus)',
                                              'relevant': False,
                                              'status': 'UNIVERSAL',
                                              'typ': 'Bool',
                                              'value': False}},
 'Total_Salary': {'Total_Salary()': {'environmental': False,
                                     'is_assignment': True,
                                     'nb_possible': -1,
                                     'normal': True,
                                     'reading': 'Total_Salary',
                                     'relevant': False,
                                     'status': 'UNIVERSAL',
                                     'typ': 'Real',
                                     'value': '696000'},
                  'Total_Salary() = sum{(salary_of_Person(Person)) | Person ∈ Person_t: true}': {'environmental': False,
                                                                                                 'is_assignment': True,
                                                                                                 'nb_possible': -1,
                                                                                                 'normal': False,
                                                                                                 'reading': 'Total_Salary '
                                                                                                            '= '
                                                                                                            'sum{(salary_of_Person(Person)) '
                                                                                                            '| Person '
                                                                                                            '∈ '
                                                                                                            'Person_t: '
                                                                                                            'true}',
                                                                                                 'relevant': False,
                                                                                                 'status': 'UNIVERSAL',
                                                                                                 'typ': 'Bool',
                                                                                                 'value': True}},
 'age_of_Person': {},
 'gender_of_Person': {},
 'salary_of_Person': {},
 'status_of_Person': {}}
