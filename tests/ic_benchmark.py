"""
Benchmark IDP-Z3 "as if its being used" in the IC by effectively user input.
This input is hard-coded per specific KB.
"""

import json
import time
from requests import post, exceptions


def launch_payload(file, meta: bool = False) -> bool:
    headers = {"Content-Type": "application/json"}
    if meta:
        url = "http://localhost:5000/meta"
    else:
        url = "http://localhost:5000/eval"
    with open(file, "r") as fp:
        payload = json.load(fp)

    try:
        return post(url, json=payload, headers=headers, timeout=60) is not None
    except exceptions.ReadTimeout:
        return False
    except exceptions.ConnectionError:
        import sys

        print("No server found -- make sure to host the back-end at localhost:5000")
        sys.exit()


def time_payloads(files):
    """
    Run the KB "as if it's running in the IC" by sending payloads.
    """
    timings = []
    for file in files:
        meta = True if "meta" in file else False
        start = time.time()
        if launch_payload(file, meta):
            diff = time.time() - start
            timings.append(diff)
            print(f"{diff}: {file}")
        else:
            timings.append("nan")
            print(f"timeout: {file}")
    return timings
