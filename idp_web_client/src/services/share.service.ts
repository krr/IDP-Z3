import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppSettings } from './AppSettings';

@Injectable({
  providedIn: 'root',
})
export class ShareService {
  constructor(private http: HttpClient) {}

  sendCompressedData(data: string): Observable<any> {
    return this.http.post<string>(AppSettings.SHARE_URL, { compressedData: data });
  }

  getSpecFile(shareCode: string): Observable<any> {
    return this.http.get(`${AppSettings.SHARE_URL}/${shareCode}`, {responseType: 'text'});
  }
}
