import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

// Observer pattern:
@Injectable({ providedIn: 'root' })
export class CommandService {
  private OutputPanelSubject = new Subject<void>();
  private DocPanelSubject = new Subject<void>();

  public OutputPanelObservable = this.OutputPanelSubject.asObservable();
  public DocPanelObservable = this.DocPanelSubject.asObservable();

  public EmitOutputPanelEvent() {
    this.OutputPanelSubject.next();
  }

  public EmitDocPanelEvent() {
    this.DocPanelSubject.next();
  }
}
