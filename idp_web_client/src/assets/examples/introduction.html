<h2>Introduction to FO(·)</h2>

<p>
This chapter briefly goes over the very basics of the <a href="https://en.wikipedia.org/wiki/FO-dot" target="_blank">FO(·) language</a>.
FO(·), read as "Eff-Oh-Dot", is a knowledge representation language based on classical first order logic.
Its main goals are to be readable, precise, and expressive.
FO(·) excels at representing complex behaviour in a largely straightforward manner.
</p>

<p>
We frequently refer to a knowledge specification in FO(·) as a "knowledge base" (KB).
Such a KB is structured in <i>blocks</i>, with each block its own purpose.
The most important of these blocks are the <i>Vocabulary</i>, <i>Theory</i> and <i>Structure</i>.
</p>

<!-- <p> -->
<!-- We'll briefly go over these blocks using a sudoku puzzle as an example. <a href="#" data-ref="intro/1a.idp"> Click here to load it in the editor.</a> -->
<!-- </p> -->

<h3> Vocabulary </h3>

<p>
In the vocabulary, we declare which symbols we will be using to express our knowledge.
For instance, a vocabulary modelling a 2x2 sudoku might look as follows:
</p>

<pre>
vocabulary {
    type Cell := {1..16}
    value_of: Cell -> Int
    same_row: Cell * Cell -> Bool
    ...
}
</pre>

<p>
Let's dissect this a little.
On the first line, we declare a type <i>Cell</i>, which you can think of as a "domain of values".
In this case, we use it to represent the cells in our grid, ranging from cell 1 to cell 16.
</p>

<p>
Next, we declare a symbol <i>value_of</i>. 
More specifically, this symbol is a <i>function</i>: it represents a mapping of each cell value on an integer number, as indicated by the <code>Cell -> Int</code> syntax.
</p>

<p>
Thirdly, we declare <i>same_row</i>, which we want to use to indicate when two cells are in the same row.
Note how this symbol does not map on Int, but on Bool (i.e., True/False).
For this reason, we call it a <i>predicate</i>.
Similarly, we introduce a <i>same_col</i> and <i>same_box</i>, but these are left out for readability.
</p>

<p>
You can already try out what the have in the editor by clicking the following link, followed by "Run" in the top menu.
  <a href="#" data-ref="intro/sudoku1.idp"> Try in the editor</a>
</p>


<h3> Structure </h3>

<p>
You'll notice that IDP-Z3 now gives 10 solutions (called "models"), but that the <i>same_xxx</i> variables are all empty. 
Of course, we actually already know the values for these variables: we know that cell 1 is in the same row as Cells 2, 3, and 4, etc.
In IDP-Z3, information that is known up-front can be expressed in the <i>structure</i> block.
</p>

<pre>
structure {
    same_row := {(1, 2), (1, 3), (1, 4),
                 (5, 6), (5, 7), (5, 8),
                 (9, 10), (9, 11), (9, 12)}.
    ...
}
</pre>


<p>
There is a catch, however: interpreting a symbol in the structure is <i>total</i>, meaning in this case that <i>same_row</i> is completely fixed and can no longer be modified.
Feel free to test it out in the editor:
  <a href="#" data-ref="intro/sudoku2.idp"> try in the editor</a>
</p>

<h3> Theory </h3>

<p>
Looking at the generated models from the previous step, you'll see that our <i>value_of</i> is completely incorrect: some cells might incorrectly have the same value, and they might also have values outside of the range of 1-4.
</p>

<p>
To fix this, the <i>theory</i> block allows us to express rules and constraints over the symbols in our problem.
These formulas are formed using connectives and operators from classical logic, such as "and" (∧), "or" (∨), "not" (¬), implication" (⇒) and universal/existential quantification (∀/∃).
For instance, to limit the value of each cell between 1 and 4, we can write:
</p>

<pre>
∀c ∈ Cell: 1 ≤ value_of(c) ≤ 4.
</pre>

<p>
In English, this can be read as "Every cell must have a value higher than or equal to 1, and lower than or equal to 4".
The "∀" symbol here stands for universal quantification, which corresponds to "For every x must..." in natural language.
</p>

<p>
We can also express the rules of sudoku in a similar way by expressing that "Every two cells which share a row/col/box should have a different value".
<p>

<pre>
∀c1, c2 ∈ Cell: same_row(c1, c2) ⇒ value_of(c1) ≠ value_of(c2).
</pre>

<p>
Using these formulas, we have completely described the rules of sudoku. :-)
  <a href="#" data-ref="intro/sudoku3.idp"> Try in the editor.</a>
</p>

<p>
You'll notice that all IDP-Z3 does is generate feasible sudokus.
While this is cool, we of course also want to be able to solve <i>existing</i> sudokus, which requires a way to tell IDP-Z3 which cell values were known up-front.
Earlier, we said that interpretations in the structure should be total (requiring a known value for every cell), but there is an exception for functions allowing us to express <i>partial interpretations</i>.
For instance, we can add an example sudoku to our structure as follows:
</p>

<pre>
value_of ⊇ {2 → 1, 3 → 4, 8 → 3, 9 → 2}.
</pre>

<p>
Now we can let IDP-Z3 solve this for us, which will find exactly one solution.
  <a href="#" data-ref="intro/sudoku4.idp"> Try in the editor.</a>
</p>

<p>
This concludes the short introduction to FO(·). In the next chapter, we will delve a bit deeper into the capabilities of IDP-Z3, and elaborate on the block called <i>main</i> that you might already have noticed.
If, instead, you want to learn more about FO(·) specifically, check the resources on the main page. 

