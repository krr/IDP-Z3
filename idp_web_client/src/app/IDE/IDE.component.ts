import { Component, ElementRef, ViewChild, HostListener, OnInit } from '@angular/core';
import { IdpService } from '../../services/idp.service';
import { Splitter } from 'primeng/splitter';
import { CommandService } from 'src/services/command.service';


@Component({
  selector: 'app-IDE',
  templateUrl: './IDE.component.html',
  styleUrls: ['./IDE.component.css']
})
export class IDEComponent implements OnInit {
  @ViewChild('splitter1', { static: false }) splitter1: Splitter;
  @ViewChild('splitter2', { static: false }) splitter2: Splitter;
  @ViewChild('editorContainer', { static: false }) editorContainer: ElementRef;
  @ViewChild('outputContainer', { static: false }) outputContainer: ElementRef;
  @ViewChild('docContainer', { static: false }) docContainer: ElementRef;
  @ViewChild('splitterContainer', { static: false }) splitterContainer: ElementRef;

  splitter1key = "splitter1";
  splitter2key = "splitter2";
  showPanel1 = false;
  showPanel2 = false;
  private minPanelSize = 10;
  private offset = 15;

  constructor(public idpService: IdpService,
    private commandService: CommandService){
  }

  @HostListener('window:mousemove', ['$event'])
  onWindowMouseMove(e: MouseEvent) {
    const leftPanelSize = this.splitter1.panelSizes[0];
    const rightPanelSize = this.splitter2.panelSizes[1];

    this.showPanel1 = leftPanelSize >= this.minPanelSize;
    this.showPanel2 = rightPanelSize >= this.minPanelSize;
    this.adjustLayout(e);
    this.changePadding();
    this.idpService.editor.layout();
  }

  ngOnInit() {
    this.commandService.OutputPanelObservable.subscribe(() => {
      if (!this.showPanel2){
        this.showOutputPanel();
      }
    });
    this.commandService.DocPanelObservable.subscribe(() => {
      this.toggleDocPanel();
    });
  }

  ngAfterViewInit(){
    const gutters = this.splitterContainer.nativeElement.querySelectorAll('.p-splitter-gutter');

    gutters[0].addEventListener('click', () => {
      this.toggleDocPanel();
    });

    gutters[1].addEventListener('click', () => {
      this.toggleOutputPanel();
    });
  }

  private showOutputPanel(){
    this.showPanel2 = true;
    this.expandOutputPanel();
    this.changePadding();
    this.idpService.editor.layout();
  }

  toggleOutputPanel() {
    this.showPanel2 = !this.showPanel2;
    if (this.showPanel2) {
      this.expandOutputPanel();
    } else {
      this.collapseOutputPanel();
    }
    this.idpService.editor.layout();
    this.changePadding();
  }

  toggleDocPanel() {
    this.showPanel1 = !this.showPanel1;
    if (this.showPanel1) {
      this.expandDocPanel();
    } else {
      this.collapseDocPanel();
    }
    this.idpService.editor.layout();
    this.changePadding();
  }

  private collapseDocPanel() {
    const totalWidth = this.splitterContainer.nativeElement.getBoundingClientRect().width - this.offset;
    const currentOutputWidth = this.outputContainer.nativeElement.getBoundingClientRect().width;

    const docWidth = 0;
    const editorWidth = totalWidth - currentOutputWidth;

    this.outputContainer.nativeElement.style.width = `${currentOutputWidth}px`;
    this.editorContainer.nativeElement.style.width = `${Math.max(0, editorWidth)}px`;
    this.docContainer.nativeElement.style.width = `${Math.max(0, docWidth)}px`;

    const newPrevPanelSize = (100 * docWidth) / totalWidth;
    const newNextPanelSize = (100 * editorWidth) / totalWidth;

    this.splitter1.panelSizes[0] = newPrevPanelSize;
    this.splitter1.panelSizes[1] = newNextPanelSize;

    localStorage.setItem(this.splitter1key, JSON.stringify(this.splitter1.panelSizes));
  }

  private expandDocPanel() {
    const totalWidth = this.splitterContainer.nativeElement.getBoundingClientRect().width - this.offset;
    const currentOutputWidth = this.outputContainer.nativeElement.getBoundingClientRect().width;

    const docWidth = totalWidth * 0.3;
    const remainingWidth = totalWidth - docWidth;
    const editorWidth = remainingWidth - currentOutputWidth;

    this.outputContainer.nativeElement.style.width = `${currentOutputWidth}px`;
    this.editorContainer.nativeElement.style.width = `${Math.max(0, editorWidth)}px`;
    this.docContainer.nativeElement.style.width = `${Math.max(0, docWidth)}px`;

    const newPrevPanelSize = (100 * docWidth) / totalWidth;
    const newNextPanelSize = (100 * editorWidth) / totalWidth;

    this.splitter1.panelSizes[0] = newPrevPanelSize;
    this.splitter1.panelSizes[1] = newNextPanelSize;

    localStorage.setItem(this.splitter1key, JSON.stringify(this.splitter1.panelSizes));
  }

  private collapseOutputPanel(){
    const totalWidth = this.splitterContainer.nativeElement.getBoundingClientRect().width - this.offset;
    const currentDocWidth = this.docContainer.nativeElement.getBoundingClientRect().width;

    const outputWidth = 0;
    const editorWidth = totalWidth - currentDocWidth;

    this.outputContainer.nativeElement.style.width = `${outputWidth}px`;
    this.editorContainer.nativeElement.style.width = `${Math.max(0, editorWidth)}px`;
    this.docContainer.nativeElement.style.width = `${Math.max(0, currentDocWidth)}px`;

    const newPrevPanelSize = (100 * editorWidth) / totalWidth;
    const newNextPanelSize = (100 * outputWidth) / totalWidth;

    this.splitter2.panelSizes[0] = newPrevPanelSize;
    this.splitter2.panelSizes[1] = newNextPanelSize;

    localStorage.setItem(this.splitter2key, JSON.stringify(this.splitter2.panelSizes));
  }

  private expandOutputPanel(){
    const totalWidth = this.splitterContainer.nativeElement.getBoundingClientRect().width - this.offset;
    const currentDocWidth = this.docContainer.nativeElement.getBoundingClientRect().width;

    const outputWidth = totalWidth * 0.3;
    const remainingWidth = totalWidth - outputWidth;
    const editorWidth = remainingWidth - currentDocWidth;

    this.outputContainer.nativeElement.style.width = `${outputWidth}px`;
    this.editorContainer.nativeElement.style.width = `${Math.max(0, editorWidth)}px`;
    this.docContainer.nativeElement.style.width = `${Math.max(0, currentDocWidth)}px`;

    const newPrevPanelSize = (100 * editorWidth) / totalWidth;
    const newNextPanelSize = (100 * outputWidth) / totalWidth;

    this.splitter2.panelSizes[0] = newPrevPanelSize;
    this.splitter2.panelSizes[1] = newNextPanelSize;

    localStorage.setItem(this.splitter2key, JSON.stringify(this.splitter2.panelSizes));
  }

  private adjustLayout(e: MouseEvent) {
    // 10 is for the margin between body and splitter component.
    const totalWidth = this.splitterContainer.nativeElement.getBoundingClientRect().width - this.offset;

    const docWidth = e.clientX;
    const fixedOutputWidth = this.outputContainer.nativeElement.getBoundingClientRect().width;
    const remainingWidth = totalWidth - docWidth - fixedOutputWidth;
    if (this.splitter1.dragging) {

      this.docContainer.nativeElement.style.width = `${Math.max(0, docWidth)}px`;
      this.editorContainer.nativeElement.style.width = `${Math.max(0, remainingWidth)}px`;
      this.outputContainer.nativeElement.style.width = `${fixedOutputWidth}px`;
    }

    const orginalDocWidth = this.docContainer.nativeElement.getBoundingClientRect().width;
    // 30 for margin and also so that the gutter (splitter) doesnt slide out the box.
    const editorWidth = e.clientX - orginalDocWidth - 30;
    const outputWidth = totalWidth - editorWidth - orginalDocWidth;
    if (this.splitter2.dragging) {
      this.docContainer.nativeElement.style.width = `${Math.max(0, orginalDocWidth)}px`;
      this.editorContainer.nativeElement.style.width = `${Math.max(0, editorWidth)}px`;
      this.outputContainer.nativeElement.style.width = `${Math.max(0, outputWidth)}px`;
    }
  }

  private changePadding() {
    const paddingValue1 = this.showPanel1 ? '10px' : '0px';
    const paddingValue2 = this.showPanel2 ? '10px' : '0px';
    this.docContainer.nativeElement.style.padding = paddingValue1;
    this.outputContainer.nativeElement.style.padding = paddingValue2;
  }
}
