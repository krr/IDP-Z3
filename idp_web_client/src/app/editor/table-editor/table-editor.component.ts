import { Component, OnInit } from '@angular/core';
import {IdpService} from '../../../services/idp.service';

@Component({
  selector: 'app-table-editor',
  templateUrl: './table-editor.component.html',
  styleUrls: ['./table-editor.component.css']
})
export class TableEditorComponent implements OnInit {

  // Labels for the table input selection.
  tableTypes = [
      { name: 'Type Glossary', value: 'typeGloss' },
      { name: 'Function Glossary', value: 'functionGloss' },
      { name: 'Predicate Glossary', value: 'predicateGloss' },
      { name: 'Constant Glossary', value: 'constantGloss' },
      { name: 'Bool Glossary', value: 'boolGloss' },
      { name: 'Goal Table', value: 'goal' },
      { name: 'Decision Table', value: 'decision' },
      { name: 'Constraint Table', value: 'constraint' },
  ];
  selectedTableType = {name: 'Decision Table', value: 'decision'};
  idpService = null;
  canvasHeight = null;


  constructor(idpService: IdpService) {
    this.idpService = idpService;
    idpService.tableEditor = this;
    this.canvasHeight = window.screen.height * 0.55;
  }

  ngOnInit() {

  }

  addTable(tableType) {
    switch (tableType) {
      case 'decision':
        this.idpService.tables.push([['Header', 'input', 'output'], ['U', 'Input', 'Output'], ['1', '', ' ']]);
        break;
      case 'constraint':
        this.idpService.tables.push([['Header', 'input', 'output'], ['E*', 'Input', 'Output'], ['1', '', ' ']]);
        break;
      case 'type':
        this.idpService.tables.push([['glossary', 'glossary', 'glossary'], ['', 'Type', ''], ['Name', 'DataType', 'Possible Values'], ]);
        break;
      case 'function':
        this.idpService.tables.push([['glossary', 'glossary'], ['Function', ''], ['Name', 'DataType'], ['', ' ']]);
        break;
      case 'relation':
        this.idpService.tables.push([['glossary'], ['Relation'], ['Name'], ['']]);
        break;
      case 'bool':
        this.idpService.tables.push([['glossary'], ['Boolean'], ['Name'], ['']]);
        break;
      case 'constant':
        this.idpService.tables.push([['glossary', 'glossary'], ['Constant', ''], ['Name', 'DataType'], ['', ' ']]);
        break;
      case 'goal':
        this.idpService.tables.push([['goal'], ['Goal'], ['Get 1 model']]);
        break;
      case 'data':
        this.idpService.tables.push([['Header', 'input', 'output'], ['D', 'Input', 'Output'], ['1', '', ' ']]);
        break;
      case true:
        console.log('Unknown table type.');
    }
  }

  delTable(idx) {
    this.idpService.tables.splice(idx, 1);
  }

  reorder(event) {
    let movedTab = this.idpService.tables[event.previousIndex];
    this.delTable(event.previousIndex);
    this.idpService.tables.splice(event.currentIndex, 0, movedTab);
  }


}
