import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'app-editable-table',
  templateUrl: './editable-table.component.html',
  styleUrls: ['./editable-table.component.css']
})


/*
 * Editable (c)DMN table component.
 * Also resizable.
 * @input datasource: an array of arrays, each representing a row.
 *   Note: the 0th array is used to denote the column types ('input', 'output', 'id', 'glossary')
 */
export class EditableTableComponent implements OnInit {
  @Input() dataSource = [];
  @Input() tableId;
  nbColumns = [];
  nbInputs = 0;
  symbolsVisible = false;
  colWidth = [];
  headerWidth = null;

  constructor() {
  }

  ngOnInit() {
    this.resizeTable();
  }


  updateCell(row, col, e) {
    this.dataSource[row][col] = (<HTMLInputElement>document.getElementById('input_' + this.tableId + '_' + row + '_' + col)).value.trim();
    this.resizeTable();
  }

  removeRow(row) {
    this.dataSource.splice(row, 1);
    if (this.dataSource[0][0] !== 'glossary' && this.dataSource[0][0] !== 'goal') {
        this.updateIndices();
    }
    this.resizeTable();
  }

  addRow(row) {
    const size = this.dataSource[0].length;
    // We don't add a blank row, because of a bug. Instead, we add `size` times a space character (' ').
    const newRow = Array.from({length: size}, (e, i) => ' '.repeat(i));
    this.dataSource.splice(row, 0, newRow);
    if (this.dataSource[0][0] !== 'glossary' && this.dataSource[0][0] !== 'goal') {
        this.updateIndices();
    }
    this.resizeTable();
  }

  updateIndices() {
    // Iterates through the rows and ensures each row has the correct index.
    // Especially important when editing the tables via inserts/deletions/etc
    for (let i = 2; i < this.dataSource.length; i++) {
       this.dataSource[i][0] = (i - 1).toString();
    }
  }

  removeCol(col) {
    for (let i = 0; i < this.dataSource.length; i++) {
      this.dataSource[i].splice(col, 1);
    }
    this.resizeTable();

  }

  addCol(col) {
    let colType = '';
    const leftCol = this.dataSource[0][col - 1];
    const rightCol = this.dataSource[0][col];
    if (col - 1  === 0 || leftCol === 'input') {
        // Entered to the right of 'id' or an existing input.
        colType = 'input';
    } else if ((leftCol === 'output' && rightCol === 'output') || col === this.dataSource[0].length) {
        // Entered between two outputs, or as last column.
        colType = 'output';
    }
    for (let i = 0; i < this.dataSource.length; i++) {
      if (i === 0) {
        this.dataSource[i].splice(col, 0,colType);
      } else if (i === 1) {
        this.dataSource[i].splice(col, 0,'newCol');
      } else {
        this.dataSource[i].splice(col, 0, '');
      }
    }
    this.resizeTable();
  }

  toInput(col) {
    this.dataSource[0][col] = 'input';
    this.resizeTable();
  }

  toOutput(col) {
    this.dataSource[0][col] = 'output';
    this.resizeTable();
  }

  showSymbols() {
    this.symbolsVisible = true;
  }

  hideSymbols() {
    this.symbolsVisible = false;
  }

  /*
   * To ensure every element is always visible, the method below automatically resizes the table.
   * Can be called whenever the contents of the table are modified.
   * It's pretty simple: find the longest element of every column, and set the column width. Ezpz.
   * It also recalculates the number of input columns, to ensure the header is always correctly placed.
   */
  resizeTable() {
    // Calculate the number of inputs for the header, and also calculate its width by summing the width of the inputs.
    this.nbInputs = this.dataSource[0].findLastIndex((x) => x === 'input');
    let newHeaderWidth = 0;

    // iterate over columns
    for (let col = 0; col < this.dataSource[0].length; col++) {
      let longest = '';
      for (let row = 1; row < this.dataSource.length; row++) {
        if (longest.length < this.dataSource[row][col].length) {
          longest = this.dataSource[row][col]
        }
      }
      // Now that we have the longest, calculate the length.
      const length = 20 + longest.length * 8
      this.colWidth[col] = length + 'px';

      // Also update the header width if the column is an input
      if (col < this.nbInputs + 1) {
        newHeaderWidth += length;
      }
    }
    this.headerWidth = newHeaderWidth + 'px';
  }
}
