import {
  Component,
  AfterViewInit,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
} from "@angular/core";
import { MessageService } from "primeng/api";
import { DOCUMENT } from "@angular/common";
import { IdpService } from "../../../services/idp.service";
import { Inject } from "@angular/core";
import {
  Child,
  Parent,
  Abstract,
  PARENT_ARC_R,
  PAD,
  defaultTypeOptions,
} from "./objects";
import { DropdownModule } from "primeng/dropdown";
import { InputTextModule } from "primeng/inputtext";
import { ButtonModule } from "primeng/button";

@Component({
  selector: "app-feature-model-editor",
  templateUrl: "./feature-model-editor.component.html",
  styleUrls: ["./feature-model-editor.component.css"],
})
export class FeatureModelEditorComponent implements AfterViewInit {
  @Output() messageEvent: EventEmitter<string> = new EventEmitter<string>();
  @ViewChild("svg", { static: false }) svgEl: ElementRef;
  @ViewChild("settings") settingsEl: ElementRef;
  @ViewChild("boxname") settingsNameEl: ElementRef;
  @ViewChild("boxtype") settingsTypeEl: ElementRef;

  public ChildEnum = Child;
  public ParentEnum = Parent;

  selectedBox = null;
  svg = null;
  boxMoving = null;
  boxClickoff = null;
  svg_top = 0;
  svg_left = 0;
  cancelSpaceClick = false;
  lines = this.svgel("g", []);
  newLines = null;

  // Options for the drop-downs.
  typeOptions = defaultTypeOptions;
  abstractOptions = [
    { label: "abstract", value: Abstract.abstract },
    { label: "concrete", value: Abstract.concrete },
  ];
  childOptions = [
    { label: "Normal child", value: Child.normal },
    { label: "Optional", value: Child.optional },
    { label: "Mandatory", value: Child.mandatory },
  ];
  parentOptions = [
    { label: "And", value: Parent.and },
    { label: "Or", value: Parent.or },
    { label: "Alternative", value: Parent.alternative },
  ];

  constructor(public idpService: IdpService) {
    this.idpService = idpService;
    this.idpService.featureModelEditor = this;
  }

  /*
   * Initializes the SVG and draws the feature model.
   */
  ngAfterViewInit() {
    this.svg = this.svgEl.nativeElement;

    /* SVG image containing one group with all lines and zero or more groups representing boxes */
    this.svg.appendChild(this.lines);
    window.addEventListener("resize", () => {
      this.fitScreenSize();
    });

    /* SVG subelements can't cancel default action... need to disable it for entire SVG */
    this.svg.oncontextmenu = function (ev) {
      ev.preventDefault();
    };
    this.fitScreenSize();
    this.boxStopEditmode();

    // Draw the feature model.
    // Note: it is important to draw here, as the ngAfterViewInit function is also called when toggling from IC back to IDE.
    this.lines = this.svgel("g", []);
    this.svg.appendChild(this.lines);
    for (const box of this.idpService.featureModel.boxes) {
      this.svg.appendChild(box.g);
      this.boxUpdateComplete(box);
      this.boxEditmode(box);
      this.boxSetEvents(box);
      // Also rebuild all connections.
      for (const conn of box.conns) {
        if (conn.box1 === box) {
          const ln = this.svgel("line", []);
          this.lines.appendChild(ln);
          conn.ln = ln;
        }
      }
      this.boxUpdateVisual(box);
    }
    this.updateTypeDropdown();
  }


  /*
   * Load a json file into the editor.
   */
  loadJson(json) {
    // First, create every box and set their parentStyle (AND, XOR or OR).
    const boxes = {};
    this.idpService.featureModel.legend = json["_type_declarations"];

    delete json["_type_declarations"]; // TODO: why is this needed?
    for (const boxName in json) {
      if (boxName === "_background") {
        continue;
      }
      const box = this.createBox(
        boxName,
        parseInt(json[boxName].x, 10),
        parseInt(json[boxName].y, 10),
        json[boxName].Type,
      );
      this.boxUpdateComplete(box);
      this.boxEditmode(box);
      const parentStyle = json[boxName].Relation;
      if (parentStyle === "AND") {
        box.parentStyle = Parent.and;
      } else if (parentStyle === "ALT") {
        box.parentStyle = Parent.alternative;
      } else {
        box.parentStyle = Parent.or;
      }
      boxes[boxName] = box;
    }

    // Then, create every connection and set the child style (MAN or OPT).
    for (const boxName of Object.keys(json)) {
      const children = json[boxName].Children;
      for (const childName of Object.keys(children)) {
        // Make connection, set the childStyle.
        this.boxConnectToggle(boxes[childName], boxes[boxName]);
        const childStyle = children[childName];
        if (json[boxName].Relation !== "AND") {
          boxes[childName].childStyle = Child.normal;
        } else if (childStyle === "OPT") {
          boxes[childName].childStyle = Child.optional;
        } else {
          boxes[childName].childStyle = Child.mandatory;
        }
      }
    }
    for (const [boxName, box] of Object.entries(boxes)) {
      this.boxUpdateVisual(box);
    }
  }

  /*
   * Generate a json file of the feature model.
   * Each node is described by 6 fields: 'Name', 'Parent', 'Children', 'Relation', 'x' and 'y'.
   */
  generateJson() {
    const json_data = {};
    json_data["_type_declarations"] = this.idpService.featureModel.legend;
    for (const box of this.idpService.featureModel.boxes) {
      const box_data = {};
      const name = box.t.firstChild.textContent;
      const relation = box.parentStyle;
      const children = {};
      let parent_name = "";
      for (const conn of box.conns) {
        if (box === conn["box1"]) {
          // If the box is listed first in a connection, set the other as parent.
          parent_name = conn["box2"].t.firstChild.textContent;
        } else {
          // If the box is listed second, add the first as child.
          const child_name = conn["box1"].t.firstChild.textContent;
          let child_style = conn["box1"].childStyle;
          if (child_style === "M") {
            child_style = "MAN";
          } else {
            child_style = "OPT";
          }
          children[child_name] = child_style;
        }
      }
      box_data["Name"] = name;
      box_data["Parent"] = parent_name;
      if (relation === "A") {
        box_data["Relation"] = "AND";
      } else if (relation === "X") {
        box_data["Relation"] = "ALT";
      } else {
        box_data["Relation"] = "OR";
      }
      box_data["Children"] = children;
      json_data[name] = box_data;
      if (box.type) {
        box_data["Type"] = box.type;
      } else {
        box_data["Type"] = "";
      }
      box_data["x"] = box.x;
      box_data["y"] = box.y;
    }
    return json_data;
  }

  /*
   * Creates a box, gives it the correct eventlisteners, and adds it to the global list of boxes.
   */
  createBox(description: string, x: number, y: number, type: string) {
    const box = this.createBoxElement(
      description,
      x,
      y,
      false,
      Child.normal,
      Parent.and,
      type,
    );
    // Set all listeners.
    this.boxSetEvents(box);

    this.boxUpdateComplete(box);
    this.idpService.featureModel.boxes.push(box);
    return box;
  }

  /*
   * Creates an svg box element.
   */
  createBoxElement(
    description: string,
    x: number,
    y: number,
    concrete: boolean,
    childStyle: Child,
    parentStyle: Parent,
    type: string,
  ) {
    const g = this.svgel("g", []); /* Logical box group */
    const t = this.svgel("text", []); /* Text field (determines box size) */
    const r = this.svgel("rect", []); /* Bordered box */
    const c = this.svgel("circle", { r: 7 }); /* Child type indicator (dot) */
    const p = this.svgel("path", []); /* Parent type indicator (arc) */
    t.appendChild(document.createTextNode(description));

    // Create span to show the type.
    const typeTspan = this.svgel("tspan", []);
    typeTspan.setAttribute("x", x.toString());
    typeTspan.setAttribute("y", y.toString());
    typeTspan.setAttribute("id", "tspan");
    t.appendChild(typeTspan);
    if (type) {
      // If there's a type present, add it to the span.
      typeTspan.appendChild(document.createTextNode("type: " + type));
    }
    g.appendChild(p);
    g.appendChild(r);
    g.appendChild(t);
    g.appendChild(c);
    this.svg.appendChild(g);
    const box = {
      x: x,
      y: y,
      g: g,
      t: t,
      r: r,
      c: c,
      p: p,
      conns: [],
      concrete: concrete,
      childStyle: childStyle,
      parentStyle: parentStyle,
      type: type,
    };
    return box;
  }

  /*
   * Creates an svg `name` with attributes `attributes`.
   */
  svgel(name: string, attributes: {}) {
    const el = document.createElementNS("http://www.w3.org/2000/svg", name);
    if (attributes !== undefined) {
      this.svgatt(el, attributes);
    }
    return el;
  }

  /*
   * Updates the attributes of an SVG element based on a dictionary of attribute names and values.
   */
  svgatt(element: SVGElement, attributes: {}) {
    for (const prop of Object.keys(attributes)) {
      element.setAttribute(prop, attributes[prop]);
    }
  }

  /*
   * Update all aspects of the box: name, type, size, etc. Also includes the visual aspects.
   */
  boxUpdateComplete(box) {
    /* Update text first, then use the bounding box of text to update the background */
    let tbb = box.t.getBBox();
    this.svgatt(box.t, {
      x: box.x - Math.floor(tbb.width / 2),
      y: box.y - Math.floor(tbb.height / 4),
    });
    if (box.type) {
      const typeTspan = box.t.querySelector("tspan");
      this.svgatt(typeTspan, {
        x: box.x - Math.floor(tbb.width / 2),
        y: box.y + Math.floor(tbb.height / 4),
      });
      typeTspan.innerHTML = "type: " + box.type;
    }
    tbb = box.t.getBBox();
    this.svgatt(box.r, {
      x: tbb.x - PAD.x / 2,
      y: tbb.y - PAD.y,
      width: tbb.width + PAD.x * 2,
      height: tbb.height + PAD.y * 2,
    });
    /* Update connected lines */
    this.boxUpdateVisual(box);
    for (const conn of box.conns) {
      // var conn = box.conns[i];
      this.boxUpdateVisual(box === conn.box1 ? conn.box2 : conn.box1);
    }
    this.updateTypeDropdown();
  }

  /*
   * Update the visual aspects of the box.
   */
  boxUpdateVisual(box) {
    const bb = box.r.getBBox();
    let has_parents = false,
      num_children = 0;
    let min_angle = Math.PI,
      max_angle = -Math.PI;
    for (const conn of box.conns) {
      // const conn = box.conns[i];
      let parent_box, child_box, pbb, cbb;
      if (conn.box1.y < conn.box2.y) {
        (parent_box = conn.box1), (child_box = conn.box2);
      } else {
        (parent_box = conn.box2), (child_box = conn.box1);
      }
      pbb = parent_box.r.getBBox();
      cbb = child_box.r.getBBox();
      const px = Math.floor(pbb.x + pbb.width / 2),
        py = pbb.y + pbb.height + 0.5;
      const cx = Math.floor(cbb.x + cbb.width / 2),
        cy = cbb.y - 0.5;
      this.svgatt(conn.ln, { x1: px, y1: py, x2: cx, y2: cy });
      if (box === parent_box) {
        const angle = Math.atan2(cx - px, cy - py);
        if (angle < min_angle) {
          min_angle = angle;
        }
        if (angle > max_angle) {
          max_angle = angle;
        }
        ++num_children;
      } else {
        has_parents = true;
      }
    }
    box.r.style.fill = box.concrete ? "#ccc" : null;
    if (box.childStyle !== Child.normal && has_parents) {
      box.c.style.fill = box.childStyle === Child.mandatory ? "#888" : "#fff";
      this.svgatt(box.c, {
        visibility: "visible",
        cx: Math.floor(bb.x + bb.width / 2),
        cy: bb.y - 0.5,
      });
    } else {
      this.svgatt(box.c, { visibility: "hidden" });
    }
    if (box.parentStyle !== Parent.and && num_children >= 2) {
      box.p.style.fill = box.parentStyle === Parent.or ? null : "none";
      const sx = Math.floor(bb.x + bb.width / 2),
        sy = bb.y + bb.height + 0.5; /* Center bottom */
      const lx =
        sx + PARENT_ARC_R * Math.sin(max_angle); /* Left starting point */
      const ly = sy + PARENT_ARC_R * Math.cos(max_angle);
      const rx =
        sx + PARENT_ARC_R * Math.sin(min_angle); /* Right ending point */
      const ry = sy + PARENT_ARC_R * Math.cos(min_angle);
      const draw =
        "M " +
        sx +
        " " +
        sy +
        " L " +
        lx +
        " " +
        ly +
        " A " +
        PARENT_ARC_R +
        " " +
        PARENT_ARC_R +
        " 0 0 1 " +
        rx +
        " " +
        ry +
        " Z";
      this.svgatt(box.p, { visibility: "visible", d: draw });
    } else {
      this.svgatt(box.p, { visibility: "hidden" });
    }
  }

  /*
   * Sets the mouse events on each box. As the boxes are svg elements, we have to add these events manually
   * in this way instead of using the standard angular approach.
   * Luckily, it works quite well.
   */
  boxSetEvents(box) {
    const ctx = this;
    // On click: enter edit mode.
    box.g.onclick = function () {
      ctx.cancelSpaceClick = true;
      ctx.boxEditmode(box);
    };
    // On mouse down: enter move mode.
    box.g.onmousedown = function (ev) {
      ctx.boxStartMove(box, ev);
    };
    // On right mouse click: toggle a connection.
    box.g.oncontextmenu = function () {
      // Only toggle a connection if a box is currently selected.
      ctx.cancelSpaceClick = false;
      if (ctx.selectedBox === null || ctx.selectedBox === box) {
        return;
      }
      ctx.boxConnectToggle(ctx.selectedBox, box);
    };
  }

  /* Toggle the connection of box_a to box_b
   * box_a is the child, box_b the parent.
   */
  boxConnectToggle(box_a, box_b) {
    /* Delete from A if they are already connected */
    let removed_connection = false;
    for (const i in box_a.conns) {
      if (!box_a.conns.hasOwnProperty(i)) {
        continue;
      }
      const conn = box_a.conns[i];
      if (conn.box1 === box_b || conn.box2 === box_b) {
        if (conn.ln != null) {
          this.lines.removeChild(conn.ln);
          conn.ln = null;
        }
        box_a.conns.splice(i, 1);
        removed_connection = true;
        break;
      }
    }
    /* Delete from B if they are already connected */
    for (const i in box_b.conns) {
      if (!box_b.conns.hasOwnProperty(i)) {
        continue;
      }
      const conn = box_b.conns[i];
      if (conn.box1 === box_a || conn.box2 === box_a) {
        if (conn.ln != null) {
          this.lines.removeChild(conn.ln);
          conn.ln = null;
        }
        box_b.conns.splice(i, 1);
        removed_connection = true;
        break;
      }
    }
    if (!removed_connection) {
      const ln = this.svgel("line", []);
      this.lines.appendChild(ln);
      const conn1 = { ln: ln, box1: box_a, box2: box_b };
      box_a.conns.push(conn1);
      box_b.conns.push(conn1);
    }
    this.boxUpdateVisual(box_a);
    this.boxUpdateVisual(box_b);
  }

  /* Connect box_a to box_b.
   * box_a is the child, box_b the parent.
   */
  boxConnect(box_a, box_b) {
    const ln = this.svgel("line", []);
    const conn1 = { ln: ln, box1: box_a, box2: box_b };
    box_a.conns.push(conn1);
    box_b.conns.push(conn1);
    this.boxUpdateVisual(box_a);
    this.boxUpdateVisual(box_b);
  }

  boxStartMove(box, ev) {
    if (this.boxMoving != null) {
      return;
    }
    this.boxMoving = box;
    window.addEventListener("mousemove", (e) => {
      this.boxMoveStep(e);
    });
    window.addEventListener("mouseup", (e) => {
      this.boxMoveStop(e);
    });
    this.cancelSpaceClick = true;
    /* Offset in the box to avoid jumping of the box */
    this.boxClickoff = {
      x: ev.clientX - this.svg_left - box.x,
      y: ev.clientY - this.svg_top - box.y,
    };
  }
  boxMoveStep(ev) {
    if (this.boxMoving == null) {
      return;
    }
    const x = ev.clientX - this.svg_left,
      y = ev.clientY - this.svg_top;
    (this.boxMoving.x = x - this.boxClickoff.x),
      (this.boxMoving.y = y - this.boxClickoff.y);
    this.boxUpdateComplete(this.boxMoving);
  }
  boxMoveStop(ev) {
    this.boxMoving = null;
  }
  boxDelete(box) {
    console.log(box);
    while (box.conns.length > 0) {
      const conn = box.conns[0];
      this.boxConnectToggle(conn.box1, conn.box2);
    }

    // If the box to delete is selected, also unselected it.
    if (box === this.selectedBox) {
      this.selectedBox = null;
    }
    this.svg.removeChild(box.g);
    for (const i in this.idpService.featureModel.boxes) {
      if (this.idpService.featureModel.boxes[i] === box) {
        this.idpService.featureModel.boxes.splice(+i, 1);
      }
    }
  }
  boxStopEditmode() {
    this.selectedBox = null;
  }

  boxEditmode(box) {
    if (this.selectedBox != null) {
      this.selectedBox.r.style.stroke = null;
    }
    /* Start edit mode for given box */
    box.r.style.stroke = "#06f";
    this.selectedBox = box;
  }

  /*
   * Creates an empty box where click.
   * Used as eventlistener on the SVG.
   */
  spaceClicked(ev) {
    if (this.cancelSpaceClick) {
      this.cancelSpaceClick = false;
      return;
    }
    const x = ev.clientX - this.svg_left,
      y = ev.clientY - this.svg_top;
    const box = this.createBox("Box name", x, y + 10, "");
    this.boxEditmode(box);
  }

  /*
   * Update the SVG to fit the available space.
   */
  fitScreenSize() {
    this.svg.setAttribute("width", Math.floor(window.innerWidth) + "px");
    this.svg.setAttribute(
      "height",
      Math.floor(window.innerHeight - 0.5 * window.innerHeight) + "px",
    );
    this.updateSvgPos();
  }

  /* Add some margin to the SVG's placement. */
  updateSvgPos() {
    const bb = this.svg.getBoundingClientRect();
    this.svg_top = bb.top + 8;
    this.svg_left = 0;
  }

  /*
   * Adds the custom-declared types to the type drop-downs
   */
  updateTypeDropdown() {
    this.typeOptions = defaultTypeOptions.slice();  // Copy the array
    for (const type of this.idpService.featureModel.legend) {
      this.typeOptions.push({ label: type["name"], value: type["name"] });
    }
  }

  /*
   * Removes a value from the legend.
   */
  removeLegendValue(idx) {
    this.idpService.featureModel.legend.splice(idx, 1);
    this.updateTypeDropdown();
  }
}
