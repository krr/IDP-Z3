export enum Child {
    normal = 'C',
    mandatory = 'M',
    optional = 'P',
}

export enum Parent {
    and = 'A',
    or = 'O',
    alternative = 'X',
}

export enum Abstract {
    abstract = 'A',
    concrete = 'C',
}

export const PAD = {x: 8, y: 6};
export const PARENT_ARC_R = 30;

export const defaultTypeOptions = [
      {label: 'no type', value: ''},
      {label: 'Integer', value: 'Int'},
      {label: 'Real', value: 'Real'},
      {label: 'Date', value: 'Date'}
  ];
