import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {DragDropModule} from '@angular/cdk/drag-drop';

import {AppComponent} from './app.component';
import {IdpService} from '../services/idp.service';
import { CompressionService } from '../services/compression.service';
import {HttpClientModule} from '@angular/common/http';
import {SliderModule} from 'primeng/slider';
import {InputSwitchModule} from 'primeng/inputswitch';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ButtonModule} from 'primeng/button';
import {HeaderComponent} from './header/header.component';
import {ListboxModule} from 'primeng/listbox';
import {SymbolComponent} from './consultant/configurator/symbol/symbol.component';
import {ConfiguratorComponent} from './consultant/configurator/configurator.component';
import { APP_BASE_HREF, CommonModule} from '@angular/common';
import { CalendarModule } from 'primeng/calendar';
import { DialogModule } from 'primeng/dialog';
import { DialogService, DynamicDialogModule } from 'primeng/dynamicdialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { MenubarModule } from 'primeng/menubar';
import { MessageModule } from 'primeng/message';
import { MessageService } from 'primeng/api';
import { MessagesModule } from 'primeng/messages';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { PanelModule } from 'primeng/panel';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { SelectButtonModule } from 'primeng/selectbutton';
import { SidebarModule } from 'primeng/sidebar';
import { SplitButtonModule } from 'primeng/splitbutton';
import { TabViewModule } from 'primeng/tabview';
import { TooltipModule } from 'primeng/tooltip';
import { SplitterModule } from 'primeng/splitter';

import {SymbolValueComponent} from './consultant/configurator/symbol/symbol-value/symbol-value.component';
import {SymbolHeaderComponent} from './consultant/configurator/symbol/symbol-header/symbol-header.component';
import {SymbolValueSelectorComponent} from './consultant/configurator/symbol/symbol-value/symbol-value-selector/symbol-value-selector.component';
import {ConfigurationService} from '../services/configuration.service';
import {ShowexplainComponent} from './consultant/configurator/showexplain/showexplain.component';
import {UndoComponent} from './consultant/configurator/showexplain/undo/undo.component';
import {LawComponent} from './consultant/configurator/showexplain/law/law.component';
import {ShowcompareComponent} from './consultant/configurator/showcompare/showcompare.component';
import {SymbolValueSelectorButtonsComponent} from './consultant/configurator/symbol/symbol-value/symbol-value-selector/symbol-value-selector-buttons/symbol-value-selector-buttons.component';
import {EditorComponent} from './editor/editor.component';
import {MonacoEditorModule, NgxMonacoEditorConfig} from 'ngx-monaco-editor';
import {ToastModule} from 'primeng/toast';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { ScrollTopModule } from 'primeng/scrolltop';

import { SymbolValueSliderComponent } from './consultant/configurator/symbol/symbol-value/symbol-value-slider/symbol-value-slider.component';
import { IDEComponent } from './IDE/IDE.component';
import { OutputPanelComponent } from './outputpanel/outputpanel.component';
import { ConsultantComponent } from './consultant/consultant.component';
import { EditableTableComponent } from './editor/table-editor/editable-table/editable-table.component';
import { TableEditorComponent } from './editor/table-editor/table-editor.component';
import { DocpanelComponent } from './docpanel/docpanel.component';
import { ChapterComponent } from './docpanel/chapter/chapter.component';
import { FeatureModelEditorComponent } from './editor/feature-model-editor/feature-model-editor.component';

export function monacoLoad() {
  // Register IDP language
  monaco.languages.register({id: 'IDP' });

  // Register a tokens provider for IDP
  monaco.languages.setMonarchTokensProvider('IDP', {
      tokenizer: {
          root: [
              // Block names
              // @ts-ignore
              [/vocabulary|theory|structure|display|procedure/, 'block'],

              // Keys
              // @ts-ignore
              [/\b(type) (?=\w)|\:=?|constructed from|\+|\.|\,|\(|\)|\{|\}|\*|;|\b(var) /, 'key'],
              // @ts-ignore
              [/(lambda)\b/, 'key'],
              // @ts-ignore
              [/\@(prefix)/, 'key'],

              // Built-in types
              // For some reason, Monaco's regex does not properly detect a leading \b (which we need to avoid highlighting e.g. ArtInt).
              // Instead, we manually enumerate the possible leading characters for our types
              // @ts-ignore
              [/→(Real|Int|Bool)\b/, 'builtin'],
              // @ts-ignore
              [/>(Real|Int|Bool)\b/, 'builtin'],
              // @ts-ignore
              [/\s(Real|Int|Bool)\b/, 'builtin'],
              // @ts-ignore
              [/(ℝ|𝔹|ℤ)/, 'builtin'],

              // Comments
              // @ts-ignore
              [/\/\/(?!www).*$/, 'comment'], // single line
              // @ts-ignore
              [/\/\*.*\*\//, 'comment'],  // multi-line comment on single line
              // @ts-ignore
              [/\/\*.*$/, 'comment', '@endMultiComment'], // multi-line comment on multiple lines

              // Annotations
              // @ts-ignore
              [/\[.*?]/, 'annotation'],

              // Prefixes
              // @ts-ignore
              [/(\w*?::)/, 'prefix'],

          ],
          endMultiComment: [
              // Until we reach the end of the multi-line comment, capture all inputs.
              // @ts-ignore
              [/.*\*\//, 'comment', '@popall'], // stop capturing when end is reached.
              // @ts-ignore
              [/.*$/, 'comment']
           ],
      },
  });

  // Define a new theme that contains only rules that match this language
  // 'key' and 'block' are vs-code defaults.
  // @ts-ignore
  monaco.editor.defineTheme('IDP', {
     base: 'vs',
     inherit: true,
     rules: [
         { token: 'block', foreground: '3300AA'},
         { token: 'builtin', foreground: 'aa0022'},
         { token: 'annotation', foreground: '0022aa'},
         { token: 'prefix', foreground: '353839'},
     ]
   });


   // Configure bracket behavior: highlight brackets and auto-close the bracket when opening one.
   const config = {'surroundingPairs':[{'open':'{','close':'}'}, {'open': '(', 'close': ')'}, {'open': '[', 'close': ']'}],
                   'autoClosingPairs':[{'open':'{','close':'}'}, {'open': '(', 'close': ')'}, {'open': '[', 'close': ']'}],
                   'brackets':[['{','}'], ['(', ')'], ['[', ']']],
                   'comments': {'lineComment': '//', 'blockComment': ['/*', '*/']}};
   // @ts-ignore
   monaco.languages.setLanguageConfiguration('IDP', config)

 }

const monacoConfig: NgxMonacoEditorConfig = {
  baseUrl: 'assets/',
  onMonacoLoad: monacoLoad,
};


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SymbolComponent,
    ConfiguratorComponent,
    SymbolValueComponent,
    SymbolHeaderComponent,
    SymbolValueSelectorComponent,
    ShowexplainComponent,
    ShowcompareComponent,
    SymbolValueSelectorButtonsComponent,
    EditorComponent,
    SymbolValueSliderComponent,
    IDEComponent,
    OutputPanelComponent,
    ConsultantComponent,
    UndoComponent,
    LawComponent,
    EditableTableComponent,
    TableEditorComponent,
    DocpanelComponent,
    ChapterComponent,
    FeatureModelEditorComponent
  ],
  imports: [
    CalendarModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    DropdownModule,
    HttpClientModule,
    FormsModule,
    SliderModule,
    InputSwitchModule,
    ButtonModule,
    ListboxModule,
    SelectButtonModule,
    PanelModule,
    SplitButtonModule,
    TooltipModule,
    SplitterModule,
    DialogModule,
    MessagesModule,
    OverlayPanelModule,
    ProgressSpinnerModule,
    MenubarModule,
    MonacoEditorModule.forRoot(monacoConfig),
    SidebarModule,
    MessageModule,
    TabViewModule,
    DynamicDialogModule,
    ToastModule,
    ScrollPanelModule,
    ScrollTopModule,
    InputTextModule,
    DragDropModule
  ],
  providers: [IdpService, ConfigurationService, MessageService,
    DialogService, CompressionService,
    {provide: APP_BASE_HREF, useValue: '/'}],
  bootstrap: [AppComponent],
})
export class AppModule {
}
