import {Component, OnInit } from '@angular/core';
import {MenuItem, MessageService} from 'primeng/api';
import {Collapse} from '../../model/Collapse';
import {Formula} from '../../model/Formula';
import {ConfigurationService} from '../../services/configuration.service';
import {Visibility} from '../../model/Visibility';
import {IdpService} from '../../services/idp.service';
import {CompressionService} from '../../services/compression.service';
import {AppSettings} from '../../services/AppSettings';
import { CommandService } from 'src/services/command.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ShareService } from 'src/services/share.service';
import { DataTypeEnum} from '../dataTypeEnum';
import { InferenceEnum } from '../inferenceEnum';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  display = false;
  showFileShare = false;
  showFileSave = false;
  showEnglish = false;
  showIntroduction = true; // Always show the introduction if one is included.
  showExpansions = false;
  showExplanation = false;
  URL = '';
  isUrlTooBig = false;
  isSensitiveCode = false;
  cDMNURL = '';
  fmURL = '';
  origin = '';
  propToggle = true;
  propButton = false;
  relToggle = true;
  relButton = false;

  items: MenuItem[] = [];
  normalItems: MenuItem[] = [];
  printItems: MenuItem[] = [];
  visibility: Visibility;
  collapse: Collapse;
  formula: Formula;

  editorStyle = {width: '50%', padding: '0px'};

  constructor(private configurationService: ConfigurationService,
              private compressionService: CompressionService,
              private commandService : CommandService,
              private shareService : ShareService,
              private notificationService : MessageService,
              public idpService: IdpService,
              private sanitizer: DomSanitizer) { // sanitizer is used in html template of this component.
    this.visibility = AppSettings.DEFAULT_VISIBILITY;
    this.collapse   = AppSettings.DEFAULT_COLLAPSE;
    this.formula    = AppSettings.DEFAULT_FORMULA;
  }

  ngOnInit() {
    // So that idpService can dynamically change the header by manipulating `this.items`.
    this.idpService.header = this;

    if (! this.idpService.IDE) {
      this.items = [
        { label: 'File',
        items: [{
            label: 'New', command: () => {
              const URL = AppSettings.SPECIFICATION_URL.replace(/specification/, 'new');
              this.idpService.reload(URL, DataTypeEnum.fodot);
              // Also load empty cDMN model, containing only type glossary.
              this.idpService.tables = [[['glossary', 'glossary', 'glossary'], ['', 'Type', ''], ['Name', 'DataType', 'Possible Values']]];

              this.display = true;
            }
          },
          {
            label: 'Share', command: () => {
              this.share(true);
            }
          }, {
            label: 'Save', command: () => {
              this.saveFile();
              this.showFileSave = true;
            }
          }]
        },
        {
          label: 'Knowledge Editor',
          command: () => {
            this.display = true;
            this.idpService.IDE = true;
          }
        },
        {
          /*      label: 'Visibility', icon: 'pi pi-fw pi-eye',
                items: [{
                  label: 'Core', command: () => {
                    this.onVisibilityChanged(Visibility.CORE);
                  }
                }, {
                  label: 'Relevant', command: () => {
                    this.onVisibilityChanged(Visibility.RELEVANT);
                  }
                }, {
                  label: 'All', command: () => {
                    this.onVisibilityChanged(Visibility.ALL);
                  }
                }]
              },
              {
          */
        label: 'View',
          items: [{
            label: 'All', command: () => {
              this.onCollapseChanged(Collapse.ALL);
            }
          }, {
            label: 'Possibly true', command: () => {
              this.onCollapseChanged(Collapse.POSSIBLE);
            }
          }, {
            label: 'Certainly true', command: () => {
              this.onCollapseChanged(Collapse.CERTAIN);
            }
          }, {
            label: 'Print', command: () => {
              this.onCollapseChanged(Collapse.PRINT);
            }
          }, {
            label: 'Formulas',
            items: [{
              label: 'View formulas',
              command: () => {
                this.onFormulaChanged(Formula.FORMULA);
              }
            },
              {
                label: 'View readings',
                command: () => {
                  this.onFormulaChanged(Formula.READING);
                }
              }
            ]
          }, {
            label: 'View theory in English', command: () => {
              this.showEnglish = true;
              this.idpService.getEnglish();
            }
          }, {
            label: 'View introduction', command: () => {
              this.showIntroduction = true;
            }
          }]
        },
        {
          label: 'Reset',
          items: [
            {
              label: 'Full',
              command: () => this.idpService.reset()
            },
            {
              label: 'System choices',
              command: () => this.idpService.resetSome(['CONSEQUENCE', 'ENV_CONSQ', 'EXPANDED'])
            }
          ]
        },
        {
          label: 'Inferences',
          items: [
            {
              label: 'Show 1 model',
              command: () => {
                this.idpService.mx();
              }
            },
            { label: 'Show all models',
              command: () => {
                this.idpService.abstract();
                this.showExpansions = true;
              }
            }
          ]
        },
        { label: 'Help',
          items: [
            { label: 'Video tutorial', command: () => window.open('/assets/Interactive_Consultant.mp4', '_blank')},
            { label: 'Reference', command: () => {
                const version = (this.idpService.versionInfo === 'local' || this.idpService.versionInfo === 'unknown')
                  ? 'latest' : this.idpService.versionInfo.replace('IDP-Z3 ', '');
                window.open('https://docs.idp-z3.be/en/' + version + '/', '_blank'); }},
            { label: 'Cheatsheet', command: () => {
                const version = (this.idpService.versionInfo === 'local' || this.idpService.versionInfo === 'unknown')
                  ? 'latest' : this.idpService.versionInfo.replace('IDP-Z3 ', '');
                window.open('https://docs.idp-z3.be/en/' + version + '/summary.html', '_blank'); }},
          ]
        }
      ];
    } else {
      this.items = [
        { label: 'File',
        items: [{
            label: 'New', command: () => {
              const URL = AppSettings.SPECIFICATION_URL.replace(/specification/, 'newIDE');
              this.idpService.reload(URL, DataTypeEnum.fodot);
              // Also load empty cDMN model, containing only type glossary.
              this.idpService.tables = [[['glossary', 'glossary', 'glossary'], ['', 'Type', ''], ['Name', 'DataType', 'Possible Values']]];
            }
          }, {
            label: 'Share', command: () => {
              this.share(false);
            }
          }, {
            label: 'Save', command: () => {
              this.saveFile();
              this.showFileSave = true;
            }
          }, {
            label: 'Examples', command: () => {
              this.commandService.EmitDocPanelEvent();
            }
          }]
        },
        {
          label: 'Interactive Consultant',
          command: () => {
            let response;
            switch (this.idpService.activeTab) {
              case 1:
                response = this.idpService.convertCDMN();
                break;
              case 2:
                response = this.idpService.convertFM();
                break;
              default:
                response = new Promise(resolve => resolve(null));  // ends immediately
                break;
            }

            // Once the FO(.) has been generated, verify it and run it in the IC.
            // If any errors are detected, show an error message instead.
            response.then(x => {
              this.idpService.lint()
              .then((data) => {
                if (data.filter(y => y['type'] === "Error").length === 0) {
                  this.idpService.toggleIDE();
                } else {
                  this.notificationService.add({severity: 'error', summary: 'Check your specification', detail: "Click 'Run' to see your error."});
                }
              });
            });
          }
        },
        {
          label: 'Run',
           items: [
              {label: 'Model expand', command: () => {
                  this.runInference(InferenceEnum.expand);
             }},
             {label: 'Propagate', command: () => {
                  this.runInference(InferenceEnum.propagate);
             }},
             {label: 'Explain', command: () => {
                  this.runInference(InferenceEnum.explain);
             }},
             {label: 'Formula', command: () => {
                  this.runInference(InferenceEnum.formula);
             }},
             {label: 'Other command', command: () => {
                  this.runInference(InferenceEnum.other);
             }},
           ]
        },
        {
          label: 'View',
          items: [
           {
            label: 'View theory in English', command: () => {
              this.showEnglish = true;
              this.idpService.getEnglish();
            }
          }]
        },
        { label: 'Help',
          items: [
            { label: 'Video tutorial', command: () => window.open('/assets/Interactive_Consultant.mp4', '_blank')},
            { label: 'Reference', command: () => {
                const version = (this.idpService.versionInfo === 'local' || this.idpService.versionInfo === 'unknown')
                  ? 'latest' : this.idpService.versionInfo.replace('IDP-Z3 ', '');
                window.open('https://docs.idp-z3.be/en/' + version + '/', '_blank'); }},
            { label: 'Cheatsheet', command: () => {
                const version = (this.idpService.versionInfo === 'local' || this.idpService.versionInfo === 'unknown')
                  ? 'latest' : this.idpService.versionInfo.replace('IDP-Z3 ', '');
                window.open('https://docs.idp-z3.be/en/' + version + '/summary.html', '_blank'); }},
          ]
        }
      ];
    }
    this.normalItems = this.items;
    this.printItems = [
      { label: 'Print',
        command: () => {
          window.print();
        }},
      { label: 'Close',
        command: () => {
          this.onCollapseChanged(Collapse.ALL);
        }},
    ];
    // this.onVisibilityChanged(this.visibility);
    this.onCollapseChanged(this.collapse);
    this.onFormulaChanged(this.formula);
  }

  runInference(inference: string) {
    let response;
    switch (this.idpService.activeTab) {
      case 1:
        response = this.idpService.convertCDMN();
        break;

      case 2:
        response = this.idpService.convertFM();
        break;

      default:
        response = new Promise(resolve => resolve(null));
    }
    response.then(x => {
      this.idpService.run(inference);
      this.commandService.EmitOutputPanelEvent();
    });

  }


/*  onVisibilityChanged(visibility: Visibility) {
    this.configurationService.setVisibility(visibility);
    const curSetting = this.items[2].items;
    for (const a of curSetting) {
      // @ts-ignore
      a.icon = '';
    }
    // @ts-ignore
    curSetting[visibility].icon = 'pi pi-fw pi-eye';
  }
  */

  onCollapseChanged(collapse: Collapse) {
    this.configurationService.setCollapse(collapse);

    if (collapse === Collapse.PRINT) {
      // Replace header by print header
      this.items = this.printItems;
    } else if (! this.idpService.IDE) {  // add icon for View menu (and not Help in IDE)
      this.items = this.normalItems;
        const curSetting = this.items[2].items;
        for (const a of curSetting) {
          // @ts-ignore
          a.icon = '';
        }

        // @ts-ignore
        curSetting[collapse].icon = 'pi pi-fw pi-eye';
    }
  }

  onFormulaChanged(formula: Formula) {
    if (! this.idpService.IDE) {
      this.configurationService.setFormula(formula);
      // @ts-ignore
      const curSetting = this.items[2].items[3].items;
      if (curSetting) {
        for (const a of curSetting) {
          // @ts-ignore
          a.icon = '';
        }
        // @ts-ignore
        curSetting[formula].icon = 'pi pi-fw pi-eye';
      }
    }
  }

  layout() {
    window['pckry'].layout();
  }

  copyURL(type) {
    let url;
    switch (type) {
      case 0:
        url = this.URL;
        break;
      case 1:
        url = this.cDMNURL;
        break;
      case 2:
        url = this.fmURL;
        break;
    }
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = url;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  /*
   * onChange function for the propagation/relevance toggle.
   *
   */
  toggleProp(e) {
    this.idpService.togglePropagation(e.checked);
  }
  toggleRel(e) {
    this.idpService.toggleRelevance(e.checked);
  }

  addPropagationButton() {
    // If the button is already present, do nothing.
    if (this.propButton) {
        return;
    }
    // Add the button on index 5.
    const propButton = {label: 'Propagate', command: () => this.idpService.doPropagation(true)};
    this.items[4].items.splice(2, 0, propButton);

    this.propButton = true;
    this.propToggle = false;
  }

  removePropagationButton() {
    // If the button is not present, return.
    if (!this.propButton) {
        return;
    }
    // Delete the button.
    console.log(this.items[4].items.splice(2, 1));

    this.propButton = false;
    this.propToggle = true;
  }

  addRelevanceButton() {
    // If the button is already present, do nothing.
    if (this.relButton) {
        return;
    }
    // Add the button on index 5.
    const relButton = {label: 'Relevance', command: () => this.idpService.doRelevance(true)};
    this.items[4].items.splice(3, 0, relButton);

    this.relButton = true;
    this.relToggle = false;
  }

  removeRelevanceButton() {
    // If the button is not present, return.
    if (!this.relButton) {
        return;
    }
    // Delete the button.
    console.log(this.items[4].items.splice(3, 1));

    this.relButton = false;
    this.relToggle = true;
  }

  saveFile() {
    const a = document.createElement('a');
    const data = this.idpService.spec.replace('\n', '\n\r');
    a.href = window.URL.createObjectURL(new Blob([data], {type: 'text/plain'}));
    a.download = 'theory.idp';
    a.click();
    window.URL.revokeObjectURL(a.href);
    a.remove();
  }

  /*
   * Regenerate the IC whenever a specification is changed.
   */
  // TODO: DEAD CODE?
  reloadFile() {
    switch(this.idpService.activeTab) {
      case 0: // FO(.) editor was open
        this.idpService.reloadMeta();
        break;
      case 1: // cDMN editor was open
        this.idpService.convertCDMN();
        this.idpService.reloadMeta();
        break;
      default:
        console.log('Error: unknown tab');
    }
    console.log(this.idpService.activeTab);
    this.idpService.reloadMeta();
  }

  private share(isIC: boolean) {
    const params = new URLSearchParams(window.location.search);
    if (params.has('file')) {
      this.isSensitiveCode = true;
    }

    this.origin = window.location.href.split('?')[0];

    // Generate FO(.) URL
    let compressedData = this.compressionService.compressString(this.idpService.spec);
    this.URL = `${this.origin}?${encodeURIComponent(compressedData)}`;
    this.URL += isIC ? '&ic=true' : '';
    this.showFileShare = true;

    // Generate cDMN URL, if any tables are present.
    if (this.idpService.tables.length > 1) {
      compressedData = this.compressionService.compressString(JSON.stringify(this.idpService.tables));
      this.cDMNURL = `${this.origin}?${encodeURIComponent(compressedData)}&ed=cdmn`;
      this.cDMNURL += isIC ? '&ic=true' : '';
    }
    // Generate FM URL, if any boxes are present.
    if (this.idpService.featureModel.boxes.length > 1) {
      compressedData = this.compressionService.compressString(JSON.stringify(this.idpService.featureModelEditor.generateJson()));
      this.fmURL = `${this.origin}?${encodeURIComponent(compressedData)}&ed=fm`;
      this.fmURL += isIC ? '&ic=true' : '';
    }


    // If cDMN or FM models are present, convert them into a URL as well.

    // let dataToCompress;
    // switch (this.idpService.activeTab) {
    //     case 0:  // FO(.)
    //         dataToCompress = this.idpService.spec;
    //         break;
    //     case 1:  // cDMN
    //         dataToCompress = JSON.stringify(this.idpService.tables);
    //         break;
    //     case 2:  // FM
    //         dataToCompress = JSON.stringify(this.idpService.featureModelEditor.generate_json());
    //         break;
    //     default:
    //         break;
    // }


    // this.URL = `${this.origin}?${encodeURIComponent(compressedData)}`;
    // if (this.idpService.activeTab === 1) {
    //     this.URL += '&ed=cdmn'
    // } else if (this.idpService.activeTab === 2) {
    //     this.URL += '&ed=fm';
    // }
    // // this.URL += this.idpService.activeTab === 1 ? '&ed=cdmn' : '';
    // this.URL += isIC ? '&ic=true' : '';
    // this.showFileShare = true;

    // varies by browser
    const MAX_URL_LENGTH = this.getMaxUrlLength();
    this.isUrlTooBig = this.URL.length > MAX_URL_LENGTH;
    if (this.isUrlTooBig) {
      this.notificationService.add({severity: 'error', summary: 'Spec File Too Big', detail: "You'll need to make the spec file smaller in order to share via URL."});
      const errorMessage = `
        <p>Error: spec file too big. Please contact us at
        <a href="mailto:krr@cs.kuleuven.be">krr@cs.kuleuven.be</a> to fix this issue.</p>`;
      console.error(errorMessage);
    }

    // https://gitlab.com/krr/IDP-Z3/-/merge_requests/370#note_2130245294
    // instead of using compressed data, send compressedData to PythonBackend,
    // pythonBackEnd provides a sharable link
    // when client receives sharable link,
    // it contacts pythonBackend to retrieve spec file.
    // is er backward compatibility nodig?
    // this.shareService.sendCompressedData(compressedData).subscribe(
    //   shareCode => {
    //     this.URL = `${this.origin}?code=${encodeURIComponent(shareCode.succes)}`;
    //     this.URL += this.idpService.activeTab === 1 ? '&ed=cdmn' : '';
    //     this.URL += isIC ? '&ic=true' : '';
    //     this.showFileShare = true;
    //   },
    //   _ => {
    //     this.notificationService.add({severity: 'error', summary: 'Something went wrong at the server.', detail: 'Please contact us at krr@cs.kuleuven.be to fix it.'});
    //     const errorMessage = `
    //     Error. Please contact us at
    //     krr@cs.kuleuven.be to fix it.`;
    //     console.error(errorMessage);
    //   }
    // );
  }

  // no browser api to fetch maximum url size.
  // https://stackoverflow.com/questions/417142/what-is-the-maximum-length-of-a-url-in-different-browsers
  // https://saturncloud.io/blog/what-is-the-maximum-length-of-a-url-in-different-browsers/
  private getMaxUrlLength(): number {
    const userAgent = navigator.userAgent.toLowerCase();
    if (userAgent.indexOf('firefox') > -1) {
        return 65000; // firefox has a limit of 65,536
    } else if (userAgent.indexOf('chrome') > -1) {
        return 2000; // chrome has a limit of 2,083
    } else if (userAgent.indexOf('safari') > -1) {
        return 80000; // safari has a limit of 80,000
    }
    return 2000; // default limit according to HTTP protocol.
  }
}
