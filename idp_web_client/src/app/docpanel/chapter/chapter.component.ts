import { HttpClient } from '@angular/common/http';
import { Component, Input } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { MessageService } from 'primeng/api';
import { AppSettings } from 'src/services/AppSettings';
import { IdpService } from 'src/services/idp.service';
import { DataTypeEnum } from '../../dataTypeEnum';

@Component({
  selector: 'app-chapter',
  templateUrl: './chapter.component.html',
  styleUrls: ['./chapter.component.css']
})
export class ChapterComponent {
  @Input() chapter: any;
  chapterContent: SafeHtml = '';

  constructor(private http: HttpClient,
    public idpService: IdpService,
    private notificationService: MessageService,
    private sanitizer: DomSanitizer) {}

  ngOnInit() {
    if (this.chapter) {
      this.loadChapterContent(this.chapter.id);
    }
  }

  loadChapterContent(chapterId: string) {
    const url = `assets/examples/${chapterId}.html`;
    this.http.get(url, { responseType: 'text' }).subscribe(
      content => {
        this.chapterContent = this.sanitizer.bypassSecurityTrustHtml(content);
      },
      _ => {
        this.notificationService.add({severity: 'error', summary: 'Model Not Obtained', detail: 'Please contact us at krr@cs.kuleuven.be to fix it.'});
        const errorMessage = `
        <p>Error: content not found. Please contact us at
        <a href="mailto:krr@cs.kuleuven.be">krr@cs.kuleuven.be</a> to fix it.</p>`;
        this.chapterContent = this.sanitizer.bypassSecurityTrustHtml(errorMessage);
      }
    );
  }

  handleLinkClick(event: MouseEvent) {
    const target = event.target as HTMLElement;
    if (target.tagName === 'A' && target.hasAttribute('data-ref')) {
      event.preventDefault();
      const examplePath = target.getAttribute('data-ref');
      // const dataType = target.getAttribute('data-type');


      let dataType = null;
      switch (target.getAttribute('data-type')) {
        case 'cdmn':
          dataType = DataTypeEnum.cdmn;
          this.idpService.activeTab = 1;
          break;
        case 'fm':
          dataType = DataTypeEnum.fm;
          this.idpService.activeTab = 2;
          break;
        default:
          dataType = DataTypeEnum.fodot;
          this.idpService.activeTab = 0;
          break;
      }

      this.loadExample(examplePath, dataType);

    }
  }

  async loadExample(filename: string, dataType: DataTypeEnum) {
    try {
      const URL = AppSettings.SPECIFICATION_URL.replace(/specification.idp/, filename);
      await this.idpService.reload(URL, dataType);
    } catch (error) {
      console.error('fail: ', error);
    }
  }
}
