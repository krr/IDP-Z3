import { Component, ViewChild, ViewContainerRef } from '@angular/core';
import { IdpService } from 'src/services/idp.service';

@Component({
  selector: 'app-docpanel',
  templateUrl: './docpanel.component.html',
  styleUrls: ['./docpanel.component.css']
})
export class DocpanelComponent {
  @ViewChild('dynamicComponent', { read: ViewContainerRef, static: false }) dynamicComponent: ViewContainerRef;

  mainScreen = true;
  selectedChapter = null;
  minPanelSize = 10;

  chapters = [
    {title: 'FO(·) introduction', id: 'introduction'},
    {title: 'IDP-Z3', id: 'idp-z3'},
    {title: 'Interactive Consultant', id: 'interactive-consultant'},
    {title: 'Advanced FO(·)', id: 'advanced'},
    {title: 'More Examples', id: 'otherexamples'},
    {title: 'cDMN', id: 'cdmn'},
    {title: 'Feature Models', id: 'fm'},
  ];

  constructor(public idpService: IdpService) {}

  selectChapter(chapter) {
    this.selectedChapter = chapter;
    this.mainScreen = false;
  }

  goBack() {
    this.selectedChapter = null;
    this.mainScreen = true;
  }
}
