import {Component, OnInit} from '@angular/core';
import {IdpService} from '../services/idp.service';
import {MessageService} from 'primeng/api';
import { Title } from '@angular/platform-browser';
import {AppSettings} from '../services/AppSettings';
import {CompressionService} from '../services/compression.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = '';
  specification_url = '';

  constructor(public idpService: IdpService,
              private messageService: MessageService,
              private titleService: Title) {
    idpService.onEmptyRelevance.subscribe(() => {
      this.messageService.add({severity: 'success', summary: 'Model Obtained', detail: 'There are no relevant symbols left'});
    });
  }

  ngOnInit() {
    this.title = this.getTitle();
    this.titleService.setTitle( this.title )
    this.specification_url = AppSettings.SPECIFICATION_URL;
    }

  public getTitle(): string {
    return this.idpService.IDE ? 'Knowledge Editor' + '\u00A0'.repeat(8) : 'Interactive Consultant';
  }

  share(type) {
    origin = window.location.href.split('?')[0];
    let URL = ''
    switch (type) {
        case 'fodot':
            URL = origin + '?' + encodeURIComponent((new CompressionService()).compressString(this.idpService.spec));
            break;
        case 'cdmn':
            const json = JSON.stringify(this.idpService.tables);
            URL = origin + '?' + encodeURIComponent((new CompressionService()).compressString(json)) + '&ed=cdmn';
            break;
        default:
            console.log("Error, unknown specification type");
            console.log(type);
    }
    // Copy to clipboard
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = URL;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }
}
