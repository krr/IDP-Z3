import {Component} from '@angular/core';
import { IdpService } from 'src/services/idp.service';

@Component({
  selector: 'app-outputpanel',
  templateUrl: './outputpanel.component.html',
  styleUrls: ['./outputpanel.component.css']
})
export class OutputPanelComponent {

  constructor(public idpService:IdpService) {
  }
}
