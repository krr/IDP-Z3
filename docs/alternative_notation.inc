.. index:: Alternative Notations

Alternative Notations
---------------------

Besides the FO(·) editor, IDP-Z3's online interface also supports some other notations to offer an alternative modeling language for specific types of problems.
These alternative languages can help lower modeling difficulty for those problems, but are not well-suited for other problem domains.

cDMN
____

cDMN is short for *constraint Decision Model and Notation*, a user-friendly and table-based alternative for a fragment of FO(·).
If you would like to learn more about it, you can find more information on `the cDMN documentation <https://cdmn.readthedocs.io/en/stable/>`_.

Feature Modeling
________________

Feature modeling is an industry standard for modeling product configurations in a visual way, by building a tree-like structure which represents all the possible configurations of a product.
For more information on feature modeling, check out its tutorial page in the IDP-Z3 web environment.
