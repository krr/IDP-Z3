.. index:: URI

URIs
++++

*New in version 0.11.1*

IDP-Z3 has support for URIs in virtually every place where you can write a string: block names, symbol names, names of type elements and constructor names.
The only exception are variable names, which cannot be written using a URI.

There are a number of reasons for introducing URIs in your FO(·) specification, but the main benefit is providing a simple way to refer to "external concepts". Additionally, it offers improved interoperability with knowledge graph technologies.

The syntax of URIs in FO(·) aims to stay as close as possible to that of the commonly used `RDF standard <https://www.w3.org/TR/turtle/>`_. 
An example of a vocabulary declaration with absolute URIs is as follows:

.. code:: text

    vocabulary V {
        type <http://www.example.org/whatever#Type>
        type Whatever
        <http://www.example.org/whatever#foo>: <http://www.example.org/whatever#Type> -> Bool
        bar: Whatever -> <http://www.example.org/whatever#Type>
        <http://www.example.org/whatever#Somethingelse>: -> Int
    }

To prevent lengthy names, you may also shorten your URIs by introducing a `prefix <https://www.w3.org/TR/turtle/#prefixed-name>`_.
The syntax to declare the prefix is as follows: ``@prefix: *name* <https://www.your.uri/something#>.``.
It must either be declared at the top of the file, in which case it is valid to use everywhere in the FO(·) specification, or at the top of a vocabulary block, in which case you may only use it inside that vocabulary and all blocks that inherit it.
The syntax to use the prefix is ``we::foo``, which is equivalent to ``<http://www.example.org/whatever#foo>``.
As an example:

.. code:: text 

    @prefix we: <http://www.example.org/whatever#>.
    vocabulary V {
        type we::Type
        we::bar: () -> we::Type
    }

.. note::

    Internally, IDP-Z3 will not expand prefixed URIs to absolute URIs, meaning that IDP-Z3 will regard the same concept expressed using an absolute and prefixed URI differently: e.g., ``we::foo ~= <http://wwww.example.org/whatever#foo>``
