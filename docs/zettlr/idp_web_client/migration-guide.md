# Migration Documentation

## dead dependencies
by first removing the dead dependencies, it will make it easier to upgrade angular.
As there would hopefully be less peer-dependencies to worry about. 

- `dev` dependency is removed, as it is not used.
by saving the file, the localhost (browser) is also automatically refreshed. 
no need for this dependency. Also, this should have been under dev-dependency.

```
idp-engine-py3.10➜  idp_web_client (refactor/angular-v10-upgrade) depcheck                                                ✭ ✱
Unused dependencies
* @angular/animations
* @angular/material
* @angular/router
* bindings
* co
* core-js
* isotope-layout
* isotope-packery
* packery
* tslib
Unused devDependencies
* git-version
Missing dependencies
* primeng: ./src/services/idp.service.ts
```

- `bindings` package is not used as no `.node` file need to be imported in this project. 

- some dependencies are present in the static file of the project under idp_web_server. 
But this means that they are not needed anymore because those static files ultimalty are copy pasted from idp_web_client. This includes the Co lib. 

- is core-js needed ? it seems to be imported in `polyfills.ts` for browser compatibility, but it is commented out. SO removed for now.  

- It seems that angular (platform-browser) needs animations and primeng-lts needs animations and router

- See also other note, but primeng-lts is not MIT licensed. Also, in idp_web_server, under static folder, there is 3rdpartylicenses.txt file, where it is pointed out that primeng-lts "SEE LICENSE IN LICENSE.md". 

## angular 7.2 to angular 8.0

source: https://angular.dev/update-guide?v=7.2-8.0&l=3

### Before you update

- In the web client code, avoid using `window` or `document` for DOM manipulation. Instead, use Angular's Renderer2 service. This approach ensures that your code remains platform-agnostic, allowing it to function correctly across different environments. Naturally if this is not meant for other platforms, then it might not be necessary. Renderer2 provides a safer and more flexible way to interact with the DOM, aligning with Angular’s best practices for platform-independent development.

### update

- use at least node v12, since odd numbered Node.js versions will not enter LTS status and the installation breaks down. So even-numbered versions and higer than or equal to 12.

execute:
>NG_DISABLE_VERSION_CHECK=1 npx @angular/cli@8 update @angular/cli@8 @angular/core@8
>The Angular CLI requires a minimum Node.js version of v18.19.
>package versions conflicts with codelyzer, @angular/core, and zone.js.
>resolve by upgrading codelyzer and zone.js
>changed to node 16 to do an install...


- update `ViewChild` or `ContentChild`. Before Angular 8, the static/dynamic query distinction wasn't explicitly required. However, starting with Angular 8+, you need to specify this explicitly using the `{ static: true | false }` option in the `@ViewChild` and `@ContentChild` decorators.

> Example: `@ContentChild('foo', {static: false}) foo !: ElementRef;` 
ibrahim: I have set them all to static. 


- switch from the native Sass compiler to the JavaScript compiler. This has been done by uninstalling `node-sass`.
> npm uninstall node-sass 


## angular 8.0 to angular 9.0

source: https://angular.dev/update-guide?v=8.0-9.0&l=3

### Before you update

- target in tsconfig.json is now set to `esnext` instead of `es5`

- updated to TypeScript 3.8

- remove entryComponents NgModules. They are no longer required with the Ivy compiler and runtime.

- change `TestBed.get` to `TestBed.inject` in spec files. 


### afer update

- primeng-lts is replaced by primeng v11.4.2

- Cannot assign value "$event" to template variable "cell". Template variables are read-only.

So everywhere in the codebase, i had to make sure that template variables are not mutated. 
for example, here below:

Since cell is a template variable created by the *ngFor directive
Angular treats it as read-only within the template, which is causing the error.
[(ngModel)]="cell" is two-way data binding, which tries to write back the new value to cell, but cell is a read-only template variable in this case. You cannot directly update it like this.

```html
    <tr>
        <th *ngFor="let cell of dataSource[1]; index as i" 
            [ngStyle]="{'width': colWidth[i]}" 
            class="cdmnHeader">

            <input [(ngModel)]="cell" 
                class="column cell"
                (change)="updateCell(1, i, $event)"
                [ngClass]="{
                'input': this.dataSource[0][i] === 'input', 
                'output': this.dataSource[0][i] === 'output', 
                'glossTitle': this.dataSource[0][i] === 'glossary', 
                'goalTitle': this.dataSource[0][i] === 'goal'}"
                name="columnHeaders"
                [ngStyle]="{'width':colWidth[i]}"
                id="input_{{this.tableId}}_1_{{i}}"
                [readonly]="this.dataSource[0][i] === 'glossary' || this.dataSource[0][i] === 'goal'"
             />
        </th>
        <th *ngIf="symbolsVisible && dataSource[0][0] === 'id'; else leftBorder" class="buttonCell"> <button (click)="addRow(2)" class="button" class="button"> <i class="pi pi-plus"></i> </button> </th>
        <ng-template #leftBorder><th></th></ng-template>
    </tr>
```

so the above is changed:
so `cell` is changed to the index element of the array `dataSource[1][i]`


### problems

here are some problems that were already present but need to be fixed.

- `resizer` is null due to a bug where the `div` is not visible till some event happens. (till `display` is set to true.) 

- `colors` are gone out of the html, colors associated with the IC, when trying to include a variable or not. 
This was due to primeng changing classnames in the html's. So those are updated. 
>see 
>https://github.com/primefaces/primeng/wiki/PrimeOne-Migration
>https://github.com/primefaces/primeng/wiki/Migration-Guide#1000

which means that PrimeIcons should be upgraded to 4.1.0 


## angular 9.0 to angular 10.2

### before update

nothing had to happen.

### after update

- change to node 14. (you need node18 only to update)
- no major problems encoutered. 

## TODO for when upgrading angular to latest version

- replace codelyzer with ESlint.