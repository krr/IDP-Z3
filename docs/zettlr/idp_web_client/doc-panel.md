# Doc-panel 

## Initial problem: 

many UI features are not accessible due to the old version of the ui lib `primeng`.
https://www.primefaces.org/primeng-v7-lts/

In particular, the splitter component was needed, which is available from primeng version 11.  

## possible fixes: 

- create a splitter component from scratch

- find a seperate lib ex. angular-split (https://angular-split.github.io/)
>add dependency and not to my satisfaction (have to really go back in version to be compatible, had to manually update the look as well) 

- upgrade primeng

### some small problems with primeNG: 

we had been using primeng lts version 7
but the lts versions require a license before installing and importing this package.
do we have that ?

https://www.primefaces.org/lts/licenses/
https://primeng.org/lts

>Do I have to purchase a license for PrimeNG?
>No, only the versions that have the -lts suffix required a paid license. Any other version is open source under MIT license.

>Can LTS releases be used in open source projects?
>No, this means violation of the license as keys cannot be shared.

for now, i have changed to using primeNG version 11 which isnt lts supported.
but is MIT licensed. 

see the license field in the following pages: 
https://www.npmjs.com/package/primeng
https://www.npmjs.com/package/primeng-lts

## solution adopted:

upgrading primeng to at least version 11,
which requires some other packages to be updated/upgraded 
which ultimaltly required angular to be upgraded as well. (to version 10)

```bash
npm info primeng@11.0.0 peerDependencies  
{  
  rxjs: '^6.0.0',  
  'zone.js': '^0.10.2',  
  '@angular/core': '^7.0.0 || ^8.0.0 || ^9.0.0 || ^10.0.0',  
  '@angular/forms': '^7.0.0 || ^8.0.0 || ^9.0.0 || ^10.0.0',  
  '@angular/common': '^7.0.0 || ^8.0.0 || ^9.0.0 || ^10.0.0'  
}  
```


## documentation for monaco editor
source : https://microsoft.github.io/monaco-editor/typedoc/interfaces/editor.IStandaloneCodeEditor.html#layout

- the editor has a built-in method for resizing which should be called when the parent container of the editor changes. 

- at this moment, there is a div container above the editor and the terminal (output)

- this will be changed with the splitter component, the same over the document panel. 


- Also, you don't need 
```
/* Auto-resize the editor to the size of its div */
idpService.editor.layout();
```
if you have enabled `automaticLayout: true`.
but it is indeed more efficient to use `.layout(),` (polling vs pushing problem.)

see : https://github.com/suren-atoyan/monaco-react/issues/89#issue-655306660

- remove minimap, bars, etc...

source : 
1) https://microsoft.github.io/monaco-editor/typedoc/interfaces/editor.IStandaloneEditorConstructionOptions.html#overviewRulerLanes
2) https://microsoft.github.io/monaco-editor/typedoc/interfaces/editor.IStandaloneEditorConstructionOptions.html#minimap


## documentation of splitter api component of primeng
* Event Emitters:
    * onResizeStart  
    * resizeEnd 


## current problems :

* janky splitter with ngx-monaco-editor if enabled with AutomaticLayout:True.

- 1st solution : use the same method as before. reduce styl width of editor whilst dragging.
- 2nd solution : search through the API for something better, 
https://github.com/primefaces/primeng/blob/master/src/app/components/splitter/splitter.ts#L470

* PrimeNG Splitter component doesn’t have a built-in button for collapsing panels

- implemented myself, once again, i find the ui library not so extensive (at least the free ones)

* in this current version of primeNG, there exist an error when dragging the splitter and dragging the cursor to the most left side of the screen. (only the left side as well.)

- I think this errors is from primeNG itself, I have looked into it for quite a bit, but i think upgrading the packages is the ultimate solution.

- To test, add a `<p-splitter>` in app.component.html and remove the existing contents, you'll see that the error is still there.
> https://github.com/primefaces/primeng/issues/10176
> https://github.com/primefaces/primeng/issues/10286


## Client Side vs Server Side Examples

The UI of the examples are stored at the client side. But the IDP files are stored at the server. 