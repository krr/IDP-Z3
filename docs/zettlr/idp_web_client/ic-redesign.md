# IC & WebID Restructuring

This doc is based on the mail for the job student project mailed by Simon Vandevelde.

## Open Site

- When you open the site, you are greeted by the Knowledge Editor.

At the moment, the front-end depends heavely on the likes of `window.location` in order to determine which pages to show. 

for example, if you put in 
`http://localhost:4201/IDE`
then you are redirected to the Knowledge Base editor. 

if you put in any other link after the last slash, 
`http://localhost:4201/blahblah`
then you are redirected to the IC view. 

We have two views, with each having a seperate endpoint. 
so, 
* `http://localhost:4201/` redirects to the KB editor (sets IDE variable to true).
* `http://localhost:4201/IDE` redirects to `http://localhost:4201/`.
* `http://localhost:4201/blahblah` redirects to `http://localhost:4201/`.
* `http://localhost:4201/?ic=true` redirects to IC view (sets IDE variable to false).
* `http://localhost:4201/?ic=false` redirects to IDE view.
* `http://localhost:4201/IDE?ic=true` redirects to IC view.
* `http://localhost:4201/IDE?ic=false` redirects to IDE view.

no /IDE or /IC anymore
The default would be to open the application in KB view, with the possibility to use a 
request parameter, for example `?ic=true`, to open the IC view.
Caution : 'old' links to /IDE should still work.

The optimal design would be to have two components, IDE and IC (which we have)
but based on the angular router, we say which component should be loaded. 
At this moment, this is done by the IDE variable and ngIf's in the app.component.html file. 

So, instead of rewriting this, i'll create a refactor issue for the idpService in which 
the godclass needs to be refactored and introduce the angular router for handling navigation. 
since obviously, the IDE variable has nothing to do with sending and receiving request from the 
idp service endpoint.

so for now, it will be solved with the same method as done until now, which is to use 
window.location. etc..

## Files

- The toolbar at the top is cleaned up a bit

* File button in the KB view that opens up the doc-panel.
* File button in the IC view redirect user to KV view that opens up the doc-panel.
* Share button in the KB view.

## Errors




## Details

> idpSpecIC key in localStorage not needed anymore. 
> certains methods become dead, such as `reloadFile` header.component.ts

## Further issues

- idpService class is become a godclass in my opinion.
it does more than contacting the idp backend endpoint. 
* it holds state which is only used for UI purposes, such as `IDE` variable. 
* spec, specIDE, specIC variables etc. 
some refactoring in that class will be needed. 
