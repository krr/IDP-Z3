idp-engine is a reasoning engine for knowledge represented using the FO(·) language.
FO(·) (aka FO-dot) is First Order logic, with various extensions to make it more expressive:  types, equality, arithmetic, inductive definitions, aggregates, and intensional objects.
The idp-engine uses the Z3 SMT solver as a back-end.

It is developed by the Knowledge Representation group at KU Leuven in Leuven, Belgium, and made available under the [GNU LGPL v3 License](https://www.gnu.org/licenses/lgpl-3.0.txt).

See more information at [www.IDP-Z3.be](https://www.IDP-Z3.be).

Contributors (alphabetical):  Bram Aerts, Ingmar Dasseville, Jo Devriendt, Marc Denecker, Matthias Van der Hallen, Pierre Carbonnelle, Simon Vandevelde

# Installation

## idp-engine

The `idp-engine` package can be installed for Python3.8 or greater using pip:

```
pip3 install idp-engine
```

You can then execute `idp-engine` from the command line. For help on how to use this tool, see `idp-engine --help`.

## Interactive Consultant

The Interactive Consultant is a generic, IDP-Z3-based interface that allows you to interact with your FO(·) knowledge bases.
Currently, it is not yet possible to install it via pip; you need to install it manually.
Luckily, it's quite a simple process. :-)

To maintain the package's dependencies, we use [poetry](https://python-poetry.org/docs/#installation). Please make sure it's installed. After that, installing the IC is as simple as:

```
git clone https://gitlab.com/krr/IDP-Z3
cd IDP-Z3
poetry install --with consultant
```

To then launch the Interactive Consultant web server, execute the following:

```
poetry run python3 main.py
```

and open your browser at http://127.0.0.1:5000.

The Web IDE is at http://127.0.0.1:5000/IDE


# Develop

## idp-engine

Development on the web client requires additional python packages to be installed:

```
poetry install idp-engine --with dev
```

You may also want to read about the [technical documentation](http://docs.idp-z3.be/en/latest/code_reference.html) and the [Development and deployment guide](https://gitlab.com/krr/IDP-Z3/-/wikis/Development-and-deployment-guide).

## Interactive Consultant

Development on the web client requires additional software to be installed.
The web client is developed in Angular, so you need to install node.
Since we need a fairly old version (v12), the easiest way to do this is by installing [nvm](https://github.com/nvm-sh/nvm).
Afterwards, install the node packages as follows:

```
cd idp_web_client
nvm install 12
npm install -g @angular/cli@7.3.10
npm ci
```

To then launch the interface of the Interactive Consultant in development mode:

```
cd idp_web_client
npm run start
```

and open your browser at http://127.0.0.1:4201. Note: this requires the Python server to also be active (see above).

# Documentation

The user manual is in the `/docs` folder and can be locally generated as follows:
~~~~
poetry run sphinx-autobuild docs docs/_build/html
~~~~
To view it, open `http://127.0.0.1:8000`

The [documentation](https://docs.IDP-Z3.be) on [readthedocs](https://readthedocs.org/projects/idp-z3/) is automatically updated from the main branch of the GitLab repository.

The [home page](https://www.IDP-Z3.be) is in the `/homepage/IDP-Z3` folder and can be locally generated using Hugo. Check the `/homepage/IDP-Z3/readme.md` for more info.

# Testing IDP-Z3

## Pipeline 

To run the tests, run `poetry run python3 test.py` from the top directory.
This will run the files in `/tests`, and verify that their output still matches the expected output.
If any files are different, you may inspect the differences using git (e.g., `git status` and `git diff`).
If these changes are normal, you must commit them or the automated CI will fail.

There is also a testing pipeline available, which can be used by running `poetry run python3 test.py pipeline`.
This will execute model expansion and propagation on every file in `/tests`.

## Benchmark 

There is also a benchmark available which you can run with `poetry run python3 test.py benchmark`.
 This will execute the files in `/tests/Benchmark` for different domain sizes.
 Whenever you have implemented a feature that might impact performance (for better or for worse), run the benchmark both on your feature branch and on the main branch to compare.


# Testing of the Interactive Consultant

We use [Cypress](https://www.cypress.io/) to test the IC.  Installation in top project folder:
~~~~
npm install cypress --save-dev
~~~~
To launch it, start the Interactive Consultant in development mode, then:
~~~~
npx cypress open
~~~~
and choose E2E testing.

# Deploy

See the instructions [here](https://gitlab.com/krr/IDP-Z3/-/wikis/Development-and-deployment-guide).
